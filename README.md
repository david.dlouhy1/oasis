# Oasis

Oasis is your gateway to streamlined development for embedded systems. Designed for budding embedded developers and startups, Oasis eliminates the complexities of infrastructure setup. With a simple YAML configuration, users articulate their requirements, allowing the platform to seamlessly prepare the necessary environment. Say goodbye to unnecessary delays in assembling Kubernetes clusters or other infrastructure components. With Oasis, focus on your code, and let the platform handle the rest. Elevate your embedded dreams with infrastructure made simple and development made easy.

# Support

## Official website

Visit the [official website](https://oasis.rodina-dlouha.cz/) of the Oasis tool. The website will acquaint you with the quick use of the tool. Additionally, the website serves as a gateway to other components of this work.

## Documentation

Oasis has supporting [documentation](https://oasis.rodina-dlouha.cz/docs/index.html) to assist you with the tool's usage. The documentation describes both the general operation of the tool and specific model usage examples.

## Development

### Env

The ``.env`` file can be used for development. Just add it to the root folder of the repository and define the necessary secrets , passwords, keys...

### Docker images

```bash
OASIS_YOCTO_PROJECT_IMAGE=registry.gitlab.com/david.dlouhy1/oasis/yocto-project:0.0.4
docker build --pull -t $OASIS_YOCTO_PROJECT_IMAGE -f ./docker/yocto-project/Dockerfile .
docker push $OASIS_YOCTO_PROJECT_IMAGE
```

```bash
go build -o ./build/
OASIS_IMAGE=registry.gitlab.com/david.dlouhy1/oasis/oasis:0.0.2
docker build --pull -t $OASIS_IMAGE -f ./docker/oasis/Dockerfile .
#docker buildx build --pull -t $OASIS_IMAGE -f ./docker/oasis/Dockerfile .
docker push $OASIS_IMAGE
```