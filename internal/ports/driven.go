// Package ports provides interfaces for interacting with different parts of the application.
package ports

import (
	"sync"

	"github.com/spf13/cobra"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

// ConsolePort defines the interface for console operations
type ConsolePort interface {
	// HandleApply handles the application of commands.
	HandleApply(cmd *cobra.Command, args []string)
	// HandleBuildProcess handles the build process using the provided task tree.
	HandleBuildProcess(taskTree *domain.TaskTree, wg *sync.WaitGroup)
	// ExportBuildProcessToFile exports the build process to a file.
	ExportBuildProcessToFile(oasis domain.Oasis, dependencyGraph domain.DAG, taskTree domain.TaskTree, wg *sync.WaitGroup)
}
