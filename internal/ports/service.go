// Package ports defines the interface ports used within the application.
package ports

import "gitlab.com/david.dlouhy1/oasis/internal/domain"

// BuilderService defines the interface for the builder service.
type BuilderService interface {
	// LoadOasis loads the Oasis from the specified path.
	LoadOasis(path string) (oasis *domain.Oasis)
	// GenerateDependencyGraph generates the dependency graph for the given Oasis.
	GenerateDependencyGraph(oasis *domain.Oasis) (dependencyGraph *domain.DAG)
	// GenerateTaskTree generates the task tree for the given Oasis and dependency graph.
	GenerateTaskTree(oasis *domain.Oasis, dependencyTree *domain.DAG) (taskTree *domain.TaskTree)
	// BuildOasis builds the Oasis according to the provided task tree.
	BuildOasis(taskTree *domain.TaskTree)
	// ExportToFile exports the Oasis, dependency graph, and task tree to a file.
	ExportToFile(oasis domain.Oasis, dependencyGraph domain.DAG, taskTree domain.TaskTree)
}
