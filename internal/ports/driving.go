// Package ports defines various ports/interfaces used within the application.
package ports

import (
	"sync"

	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

// VCPPort defines the interface for version control platform operations.
type VCPPort interface {
	// CreateProject creates a new project with the given name and type.
	CreateProject(name string, projectType domain.ProjectType) (project *domain.GitProject, err error)
	// GetProject retrieves an existing project by name.
	GetProject(name string) (project *domain.GitProject, err error)
	// DeleteProject deletes the specified project.
	DeleteProject(project *domain.GitProject) error
}

// FSPort defines the interface for file system operations.
type FSPort interface {
	// GetOasisFiles retrieves Oasis files from the specified directory path.
	GetOasisFiles(dirPath string, fileChan chan<- string, errorChan chan error, wg *sync.WaitGroup)
	// LoadOasisFiles loads Oasis files into the provided Oasis structure.
	LoadOasisFiles(oasis *domain.Oasis, fileChan <-chan string, errorChan chan error)
	// ExportOasisDependencyToFile exports the Oasis dependency graph to a file.
	ExportOasisDependencyToFile(oasis *domain.Oasis, dependencyGraph *domain.DAG)
	// ExportOasisTaskTreeToFile exports the Oasis task tree to a file.
	ExportOasisTaskTreeToFile(oasis *domain.Oasis, TaskTree *domain.TaskTree)
	// GenerateSSHKeyPair generates an SSH key pair.
	GenerateSSHKeyPair() (err error)
	// GetPublicSSHKey retrieves the public SSH key.
	GetPublicSSHKey() (publicKey string, err error)
}

// InfraPort defines the interface for infrastructure operations.
type InfraPort interface {
	// CreateMachine creates a new machine in the infrastructure.
	CreateMachine()
}

// PlanPort defines the interface for plan operations.
type PlanPort interface {
	// CreatePlan creates a new plan with the given name.
	CreatePlan(name string) (project *domain.Plan, err error)
}
