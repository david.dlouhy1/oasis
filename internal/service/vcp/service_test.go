// Package service provides implementations of various services.
package service

import (
	"reflect"
	"testing"

	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
)

func TestNewVCPService(t *testing.T) {
	type args struct {
		vcp ports.VCPPort
	}
	tests := []struct {
		name string
		args args
		want *VCPService
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewVCPService(tt.args.vcp); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewVCPService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVCPService_CreateAndDeleteProject(t *testing.T) {
	type fields struct {
		vcp ports.VCPPort
	}
	type args struct {
		name        string
		projectType domain.ProjectType
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := VCPService{
				vcp: tt.fields.vcp,
			}
			if err := svc.CreateAndDeleteProject(tt.args.name, tt.args.projectType); (err != nil) != tt.wantErr {
				t.Errorf("VCPService.CreateAndDeleteProject() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestVCPService_CreateProject(t *testing.T) {
	type fields struct {
		vcp ports.VCPPort
	}
	type args struct {
		name        string
		projectType domain.ProjectType
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantProject *domain.GitProject
		wantErr     bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := VCPService{
				vcp: tt.fields.vcp,
			}
			gotProject, err := svc.CreateProject(tt.args.name, tt.args.projectType)
			if (err != nil) != tt.wantErr {
				t.Errorf("VCPService.CreateProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProject, tt.wantProject) {
				t.Errorf("VCPService.CreateProject() = %v, want %v", gotProject, tt.wantProject)
			}
		})
	}
}

func TestVCPService_DeleteProject(t *testing.T) {
	type fields struct {
		vcp ports.VCPPort
	}
	type args struct {
		project *domain.GitProject
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := VCPService{
				vcp: tt.fields.vcp,
			}
			if err := svc.DeleteProject(tt.args.project); (err != nil) != tt.wantErr {
				t.Errorf("VCPService.DeleteProject() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
