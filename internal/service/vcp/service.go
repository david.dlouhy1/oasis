// Package service provides implementations of various services.
package service

import (
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
)

// VCPService implements the version control platform service.
type VCPService struct {
	vcp ports.VCPPort
}

// NewVCPService creates a new instance of VCPService.
func NewVCPService(vcp ports.VCPPort) *VCPService {
	return &VCPService{
		vcp: vcp,
	}
}

// CreateAndDeleteProject creates and deletes a project.
func (svc VCPService) CreateAndDeleteProject(name string, projectType domain.ProjectType) (err error) {
	project, err := svc.CreateProject(name, projectType)
	if err != nil {
		return err
	}
	err = svc.DeleteProject(project)
	return
}

// CreateProject creates a new project.
func (svc VCPService) CreateProject(name string, projectType domain.ProjectType) (project *domain.GitProject, err error) {
	project, err = svc.vcp.CreateProject(name, projectType)
	return
}

// DeleteProject deletes an existing project.
func (svc VCPService) DeleteProject(project *domain.GitProject) (err error) {
	err = svc.vcp.DeleteProject(project)
	return
}
