// Package service provides various services for building infrastructure.
package service

import (
	"reflect"
	"sync"
	"testing"

	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
)

func TestNewBuilderService(t *testing.T) {
	type args struct {
		fsp ports.FSPort
	}
	tests := []struct {
		name string
		args args
		want *BuilderService
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewBuilderService(tt.args.fsp); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBuilderService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBuilderService_LoadOasis(t *testing.T) {
	type fields struct {
		fsp ports.FSPort
	}
	type args struct {
		path string
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantOasis *domain.Oasis
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := BuilderService{
				fsp: tt.fields.fsp,
			}
			if gotOasis := svc.LoadOasis(tt.args.path); !reflect.DeepEqual(gotOasis, tt.wantOasis) {
				t.Errorf("BuilderService.LoadOasis() = %v, want %v", gotOasis, tt.wantOasis)
			}
		})
	}
}

func TestBuilderService_GenerateDependencyGraph(t *testing.T) {
	type fields struct {
		fsp ports.FSPort
	}
	type args struct {
		oasis *domain.Oasis
	}
	tests := []struct {
		name                string
		fields              fields
		args                args
		wantDependencyGraph *domain.DAG
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := BuilderService{
				fsp: tt.fields.fsp,
			}
			if gotDependencyGraph := svc.GenerateDependencyGraph(tt.args.oasis); !reflect.DeepEqual(gotDependencyGraph, tt.wantDependencyGraph) {
				t.Errorf("BuilderService.GenerateDependencyGraph() = %v, want %v", gotDependencyGraph, tt.wantDependencyGraph)
			}
		})
	}
}

func Test_detectVertexes(t *testing.T) {
	type args struct {
		oasis           *domain.Oasis
		dependencyGraph *domain.DAG
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			detectVertexes(tt.args.oasis, tt.args.dependencyGraph)
		})
	}
}

func Test_technologiesToVertexes(t *testing.T) {
	type args struct {
		oasis           *domain.Oasis
		dependencyGraph *domain.DAG
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			technologiesToVertexes(tt.args.oasis, tt.args.dependencyGraph)
		})
	}
}

func Test_projectsToVertexes(t *testing.T) {
	type args struct {
		oasis           *domain.Oasis
		dependencyGraph *domain.DAG
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			projectsToVertexes(tt.args.oasis, tt.args.dependencyGraph)
		})
	}
}

func Test_detectEdges(t *testing.T) {
	type args struct {
		oasis           *domain.Oasis
		dependencyGraph *domain.DAG
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			detectEdges(tt.args.oasis, tt.args.dependencyGraph)
		})
	}
}

func Test_projectsToEdges(t *testing.T) {
	type args struct {
		oasis           *domain.Oasis
		dependencyGraph *domain.DAG
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			projectsToEdges(tt.args.oasis, tt.args.dependencyGraph)
		})
	}
}

func TestBuilderService_GenerateTaskTree(t *testing.T) {
	type fields struct {
		fsp ports.FSPort
	}
	type args struct {
		oasis          *domain.Oasis
		dependencyTree *domain.DAG
	}
	tests := []struct {
		name         string
		fields       fields
		args         args
		wantTaskTree *domain.TaskTree
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := BuilderService{
				fsp: tt.fields.fsp,
			}
			if gotTaskTree := svc.GenerateTaskTree(tt.args.oasis, tt.args.dependencyTree); !reflect.DeepEqual(gotTaskTree, tt.wantTaskTree) {
				t.Errorf("BuilderService.GenerateTaskTree() = %v, want %v", gotTaskTree, tt.wantTaskTree)
			}
		})
	}
}

func Test_bfs(t *testing.T) {
	type args struct {
		vertexes []*domain.DAGVertex
		dag      *domain.DAG
		taskTree *domain.TaskTree
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			bfs(tt.args.vertexes, tt.args.dag, tt.args.taskTree)
		})
	}
}

func TestBuilderService_BuildOasis(t *testing.T) {
	type fields struct {
		fsp ports.FSPort
	}
	type args struct {
		taskTree *domain.TaskTree
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := BuilderService{
				fsp: tt.fields.fsp,
			}
			svc.BuildOasis(tt.args.taskTree)
		})
	}
}

func Test_topSort(t *testing.T) {
	type args struct {
		taskTree *domain.TaskTree
		taskChan chan<- *domain.Task
		wg       *sync.WaitGroup
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			topSort(tt.args.taskTree, tt.args.taskChan, tt.args.wg)
		})
	}
}

func Test_doTasks(t *testing.T) {
	type args struct {
		taskChan <-chan *domain.Task
		wg       *sync.WaitGroup
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			doTasks(tt.args.taskChan, tt.args.wg)
		})
	}
}

func Test_doTask(t *testing.T) {
	type args struct {
		task *domain.Task
		wg   *sync.WaitGroup
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			doTask(tt.args.task, tt.args.wg)
		})
	}
}

func TestBuilderService_ExportToFile(t *testing.T) {
	type fields struct {
		fsp ports.FSPort
	}
	type args struct {
		oasis           domain.Oasis
		dependencyGraph domain.DAG
		taskTree        domain.TaskTree
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := BuilderService{
				fsp: tt.fields.fsp,
			}
			svc.ExportToFile(tt.args.oasis, tt.args.dependencyGraph, tt.args.taskTree)
		})
	}
}
