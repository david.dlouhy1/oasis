// Package service provides various services for building infrastructure.
package service

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"

	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/fs"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/infra/hetzner"
	jira "gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/plan"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/vcp/bitbucket"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/vcp/github"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/vcp/gitlab"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
	svcp "gitlab.com/david.dlouhy1/oasis/internal/service/plan"
	service "gitlab.com/david.dlouhy1/oasis/internal/service/vcp"
)

// BuilderService provides functionalities for building infrastructure.
type BuilderService struct {
	fsp ports.FSPort
}

// NewBuilderService creates a new instance of BuilderService.
func NewBuilderService(fsp ports.FSPort) *BuilderService {
	return &BuilderService{
		fsp: fsp,
	}
}

// LoadOasis loads the Oasis structure from a specified path.
func (svc BuilderService) LoadOasis(path string) (oasis *domain.Oasis) {
	fileChan := make(chan string)
	errorChan := make(chan error)

	defer close(errorChan)

	var wg sync.WaitGroup
	oasis = &domain.Oasis{}

	go func() {
		for err := range errorChan {
			if err != nil {
				if err.Error() == "not oasis file" {
					log.Println(err)
					continue
				}
				log.Fatalln(err)
			}
		}
	}()
	wg.Add(2)
	go func() {
		defer wg.Done()
		svc.fsp.LoadOasisFiles(oasis, fileChan, errorChan)
	}()

	go func() {
		defer close(fileChan)
		svc.fsp.GetOasisFiles(filepath.Dir(path), fileChan, errorChan, &wg)
	}()
	wg.Wait()

	return
}

// GenerateDependencyGraph analyzes dependencies and constructs a dependency graph.
func (svc BuilderService) GenerateDependencyGraph(oasis *domain.Oasis) (dependencyGraph *domain.DAG) {
	dependencyGraph = domain.NewDAG()

	detectVertexes(oasis, dependencyGraph)
	detectEdges(oasis, dependencyGraph)

	return
}

// DetectVertexes creates vertices in the dependency graph.
func detectVertexes(oasis *domain.Oasis, dependencyGraph *domain.DAG) {

	technologiesToVertexes(oasis, dependencyGraph)
	projectsToVertexes(oasis, dependencyGraph)

}

// TechnologiesToVertexes converts technologies to vertices in the dependency graph.
func technologiesToVertexes(oasis *domain.Oasis, dependencyGraph *domain.DAG) {
	for _, technology := range oasis.Technologies.Technologies {
		dependencyGraph.AddVertex(technology)
	}
}

// ProjectsToVertexes converts projects to vertices in the dependency graph.
func projectsToVertexes(oasis *domain.Oasis, dependencyGraph *domain.DAG) {

	for _, project := range oasis.Projects.Projects {
		dependencyGraph.AddVertex(project)
	}
}

// DetectEdges creates edges in the dependency graph.
func detectEdges(oasis *domain.Oasis, dependencyGraph *domain.DAG) {
	projectsToEdges(oasis, dependencyGraph)
}

// ProjectsToEdges creates edges between projects and technologies in the dependency graph.
func projectsToEdges(oasis *domain.Oasis, dependencyGraph *domain.DAG) {

	for _, project := range oasis.Projects.Projects {
		var projectVertex *domain.DAGVertex
		founded := false
	outa:
		for _, vertex := range dependencyGraph.Vertexes {
			if vertexValue, ok := vertex.Value.(domain.Project); ok {
				if reflect.DeepEqual(vertexValue, project) {
					projectVertex = vertex
					break outa
				}
			}

		}
		if projectVertex == nil {
			panic("missing technology vertex")
		}

		for _, technology := range oasis.Technologies.Technologies {

		outb:
			for _, techType := range technology.Type {
				switch techType {
				case "vcs":
					if strings.EqualFold(project.VCS, technology.Name) {
						var technologyVertex *domain.DAGVertex
						for _, vertex := range dependencyGraph.Vertexes {
							if vertexValue, ok := vertex.Value.(domain.Technology); ok {
								if reflect.DeepEqual(vertexValue, technology) {
									technologyVertex = vertex
									dependencyGraph.AddEdge(technologyVertex, projectVertex, "vcs")
									founded = true
									break outb
								}
							}

						}
						if technologyVertex == nil {
							panic("missing technology vertex")
						}
					}
				case "infra":
					if strings.EqualFold(project.Infra, technology.Name) {
						var technologyVertex *domain.DAGVertex
						for _, vertex := range dependencyGraph.Vertexes {
							if vertexValue, ok := vertex.Value.(domain.Technology); ok {
								if reflect.DeepEqual(vertexValue, technology) {
									technologyVertex = vertex
									dependencyGraph.AddEdge(technologyVertex, projectVertex, "infra")
									founded = true
									break outb
								}
							}

						}
						if technologyVertex == nil {
							panic("missing technology vertex")
						}
					}
				case "plan":
					if strings.EqualFold(project.Plan, technology.Name) {
						var technologyVertex *domain.DAGVertex
						for _, vertex := range dependencyGraph.Vertexes {
							if vertexValue, ok := vertex.Value.(domain.Technology); ok {
								if reflect.DeepEqual(vertexValue, technology) {
									technologyVertex = vertex
									dependencyGraph.AddEdge(technologyVertex, projectVertex, "plan")
									founded = true
									break outb
								}
							}

						}
						if technologyVertex == nil {
							panic("missing technology vertex")
						}
					}
				case "monitor":
					if strings.EqualFold(project.Monitor, technology.Name) {
						var technologyVertex *domain.DAGVertex
						for _, vertex := range dependencyGraph.Vertexes {
							if vertexValue, ok := vertex.Value.(domain.Technology); ok {
								if reflect.DeepEqual(vertexValue, technology) {
									technologyVertex = vertex
									dependencyGraph.AddEdge(technologyVertex, projectVertex, "monitor")
									founded = true
									break outb
								}
							}

						}
						if technologyVertex == nil {
							panic("missing technology vertex")
						}
					}
				default:
					panic("not implemented")
				}
			}
		}
		if !founded {
			panic("missing technology vertex")
		}
	}
}

// GenerateTaskTree generates a task tree from an Oasis and dependency tree.
func (svc BuilderService) GenerateTaskTree(oasis *domain.Oasis, dependencyTree *domain.DAG) (taskTree *domain.TaskTree) {
	taskTree = domain.NewTaskTree()

	rootVertexes := dependencyTree.FindRootNode()

	bfs(rootVertexes, dependencyTree, taskTree)

	return
}

// QueueVertex represents a vertex in the BFS queue.
type QueueVertex struct {
	Vertex     *domain.DAGVertex // Current node
	Parent     *domain.DAGVertex // Previous parent
	Edge       *domain.DAGEdge   // Edge between current vertex and parent
	ParentTask *domain.Task      // Task to be attached to
}

// BFS algorithm with conversion to a task tree.
func bfs(vertexes []*domain.DAGVertex, dag *domain.DAG, taskTree *domain.TaskTree) {

	queue := []QueueVertex{}
	initTask := domain.NewTask("do init root task", func() { fmt.Println("START") })
	taskTree.AddTask(initTask)

	for _, vertex := range vertexes {
		queue = append(queue, QueueVertex{Vertex: vertex, Parent: nil, Edge: nil, ParentTask: initTask})
	}

	for len(queue) > 0 {
		currentNode := queue[0]
		queue = queue[1:]

		current := currentNode.Vertex
		parent := currentNode.Parent
		edge := currentNode.Edge
		tasks := domain.NewTaskGroup()
		var parentTask *domain.Task

		switch current.Value.(type) {
		case domain.Project:
			switch parent.Value.(type) {
			case domain.Technology:
				technology := parent.Value.(domain.Technology)
				parentTask = currentNode.ParentTask
				switch edge.Type {
				case "vcs":
					var err error
					var adapter ports.VCPPort
					project := current.Value.(domain.Project)

					switch strings.ToLower(technology.Name) {
					case "gitlab":
						token := technology.Definition["token"].(string)
						adapter, err = gitlab.NewAdapter(token)
					case "github":
						token := technology.Definition["token"].(string)
						adapter, err = github.NewAdapter(token)
					case "bitbucket":
						username := technology.Definition["username"].(string)
						password := technology.Definition["password"].(string)
						wn := "daviddlouhy1tul"
						adapter, err = bitbucket.NewAdapter(username, password, wn)
					default:
						panic("not implemented")
					}
					if err != nil {
						log.Fatalln(err)
					}
					service := service.NewVCPService(adapter)
					task := domain.NewTask("create repository\n"+project.Name, func() {
						_, err := service.CreateProject(project.Name, project.Type)
						if err != nil {
							log.Fatalln(err)
						}
					})
					tasks.Tasks = append(tasks.Tasks, task)
				case "plan":
					var err error
					var adapter ports.PlanPort
					project := current.Value.(domain.Project)
					switch strings.ToLower(technology.Name) {
					case "jira":
						token := technology.Definition["token"].(string)
						url := technology.Definition["url"].(string)
						mail := technology.Definition["mail"].(string)
						adapter, err = jira.NewAdapter(token, url, mail)
					default:
						panic("not implemented")
					}
					if err != nil {
						log.Fatalln(err)
					}
					service := svcp.NewPlanService(adapter)
					task := domain.NewTask("create plan for\n"+project.Name, func() {
						_, err := service.CreatePlan(project.Name)
						if err != nil {
							log.Fatalln(err)
						}
					})
					tasks.Tasks = append(tasks.Tasks, task)
				case "monitor":

					switch strings.ToLower(technology.Name) {
					case "prometheus":
						task := domain.NewTask("prepare monitoring for\n"+current.Value.(domain.Project).Name, func() { fmt.Println("MONITORING_TASK") })
						tasks.Tasks = append(tasks.Tasks, task)
					default:
						panic("not implemented")
					}
				case "infra":
					task := domain.NewTask("create infra for\n"+current.Value.(domain.Project).Name, func() { fmt.Println("TECH_INFRA_TASK") })

					taskTree.AddTask(task)
					taskTree.AddLink(domain.NewLink(parentTask, task))

					childVertexes := dag.GetChildVertexes(current)
					for _, child := range childVertexes {
						queue = append(queue, QueueVertex{Vertex: child, Parent: current, Edge: dag.GetEdge(current, child), ParentTask: task})
					}

					buildFunction := func() {
						fmt.Println("TECH_INFRA_TASK")
					}
					switch strings.ToLower(technology.Name) {
					case "hetzner":
						hetznerToken := technology.Definition["token"].(string)
						buildFunction = func() {
							hAdapter, err := hetzner.NewAdapter(hetznerToken)
							if err != nil {
								log.Fatalln(err)
							}

							fsa := fs.NewAdapter()
							err = fsa.GenerateSSHKeyPair()
							if err != nil {
								log.Fatalln(err)
							}

							publicKey, err := fsa.GetPublicSSHKey()
							if err != nil {
								log.Fatalln(err)
							}
							privateKey, err := fsa.GetPrivateSSHKey()
							if err != nil {
								log.Fatalln(err)
							}

							err = hAdapter.SetPublicSSHKey(publicKey)
							if err != nil && !strings.Contains(err.Error(), "already used") {
								log.Fatalln(err)
							}

							gitLabToken := os.Getenv("OASIS_GITLAB_TOKEN")

							vca, err := gitlab.NewAdapter(gitLabToken)
							if err != nil {
								log.Fatalln(err)
							}

							prjName := current.Value.(domain.Project).Name

							prj, err := vca.GetProject(prjName)
							if err != nil {
								log.Fatalln(err)
							}

							runner, err := vca.CreateRunner(prj)
							if err != nil {
								log.Fatalln(err)
							}
							projectSrvResources, err := hAdapter.CreateServer(privateKey, publicKey, runner)
							if err != nil && !strings.Contains(err.Error(), "server name is already used") {
								log.Fatalln(err)
							}
							err = vca.SetProjectVariables(projectSrvResources, prj)
							if err != nil {
								log.Fatalln(err)
							}
						}
					}

					buildTask := domain.NewTask("create build infra for\n"+current.Value.(domain.Project).Name, buildFunction)

					taskTree.AddTask(buildTask)
					taskTree.AddLink(domain.NewLink(task, buildTask))

					childVertexes = dag.GetChildVertexes(current)
					for _, child := range childVertexes {
						queue = append(queue, QueueVertex{Vertex: child, Parent: current, Edge: dag.GetEdge(current, child), ParentTask: buildTask})
					}

					monitorTask := domain.NewTask("create monitor infra for\n"+current.Value.(domain.Project).Name, func() { fmt.Println("TECH_INFRA_TASK") })

					taskTree.AddTask(monitorTask)
					taskTree.AddLink(domain.NewLink(task, monitorTask))

					childVertexes = dag.GetChildVertexes(current)
					for _, child := range childVertexes {
						queue = append(queue, QueueVertex{Vertex: child, Parent: current, Edge: dag.GetEdge(current, child), ParentTask: monitorTask})
					}

					deployTask := domain.NewTask("create deploy infra for\n"+current.Value.(domain.Project).Name, func() { fmt.Println("TECH_INFRA_TASK") })

					taskTree.AddTask(deployTask)
					taskTree.AddLink(domain.NewLink(task, deployTask))

					childVertexes = dag.GetChildVertexes(current)
					for _, child := range childVertexes {
						queue = append(queue, QueueVertex{Vertex: child, Parent: current, Edge: dag.GetEdge(current, child), ParentTask: deployTask})
					}

					testTask := domain.NewTask("create test infra for\n"+current.Value.(domain.Project).Name, func() { fmt.Println("TECH_INFRA_TASK") })

					taskTree.AddTask(testTask)
					taskTree.AddLink(domain.NewLink(buildTask, testTask))

					childVertexes = dag.GetChildVertexes(current)
					for _, child := range childVertexes {
						queue = append(queue, QueueVertex{Vertex: child, Parent: current, Edge: dag.GetEdge(current, child), ParentTask: testTask})
					}
				default:
					panic("not implemented")
				}
			default:
				panic("not implemented")
			}
		case domain.Technology:
			technology := current.Value.(domain.Technology)
			parentTask = currentNode.ParentTask
			switch strings.ToLower(technology.Name) {
			case "gitlab":
				task := domain.NewTask("prepare\n"+current.Value.(domain.Technology).Name, func() { fmt.Println("TECH_TASK") })
				tasks.Tasks = append(tasks.Tasks, task)
			case "github":
				task := domain.NewTask("prepare\n"+current.Value.(domain.Technology).Name, func() { fmt.Println("TECH_TASK") })
				tasks.Tasks = append(tasks.Tasks, task)
			case "bitbucket":
				task := domain.NewTask("prepare\n"+current.Value.(domain.Technology).Name, func() { fmt.Println("TECH_TASK") })
				tasks.Tasks = append(tasks.Tasks, task)
			case "vmware":
				task := domain.NewTask("prepare\n"+current.Value.(domain.Technology).Name, func() { fmt.Println("TECH_INFRA_TASK") })
				tasks.Tasks = append(tasks.Tasks, task)
			case "jira":
				task := domain.NewTask("prepare\n"+current.Value.(domain.Technology).Name, func() { fmt.Println("TECH_PLAN_TASK") })
				tasks.Tasks = append(tasks.Tasks, task)
			case "hetzner":
				task := domain.NewTask("prepare\n"+current.Value.(domain.Technology).Name, func() { fmt.Println("TECH_INFRA_TASK") })
				tasks.Tasks = append(tasks.Tasks, task)
			case "prometheus":
				task := domain.NewTask("prepare\n"+current.Value.(domain.Technology).Name, func() { fmt.Println("TECH_MONITOR_TASK") })
				tasks.Tasks = append(tasks.Tasks, task)
			default:
				panic("not implemented")
			}
		default:
			panic("not implemented")
		}

		for _, task := range tasks.Tasks {
			taskTree.AddTask(task)
			taskTree.AddLink(domain.NewLink(parentTask, task))
			childVertexes := dag.GetChildVertexes(current)
			for _, child := range childVertexes {
				queue = append(queue, QueueVertex{Vertex: child, Parent: current, Edge: dag.GetEdge(current, child), ParentTask: task})
			}
		}
	}
}

// BuildOasis initiates the building process for the Oasis based on the provided task tree.
func (svc BuilderService) BuildOasis(taskTree *domain.TaskTree) {
	fmt.Println("Building...")
	wg := &sync.WaitGroup{}
	taskChan := make(chan *domain.Task)
	wg.Add(2)
	go topSort(taskTree, taskChan, wg)
	go doTasks(taskChan, wg)
	wg.Wait()
	fmt.Println("Builded")
}

// topSort performs topological sorting on the task tree and sends tasks to the channel for execution.
func topSort(taskTree *domain.TaskTree, taskChan chan<- *domain.Task, wg *sync.WaitGroup) {
	defer wg.Done()
	defer close(taskChan)
	fmt.Println("Starting create queue")
	actualLevel := 0
	for taskTree.GetTasksCount() > 0 {
		for task, parentsCount := range taskTree.GetNumbersOfParents() {
			if parentsCount == 0 {
				task.Level = actualLevel
				taskChan <- task
				for _, link := range taskTree.GetChildLinks(task) {
					taskTree.RemoveLink(link)
				}
				taskTree.RemoveTask(task)
			}
		}
		actualLevel++
	}
	fmt.Println("Queue finished")
}

// topSort performs topological sorting on the task tree and sends tasks to the channel for execution.
func doTasks(taskChan <-chan *domain.Task, wg *sync.WaitGroup) {
	defer wg.Done()
	actualLevel := 0
	wgi := &sync.WaitGroup{}
	for task := range taskChan {
		if actualLevel == task.Level {
			wgi.Add(1)
			go doTask(task, wgi)
		} else {
			wgi.Wait()
			actualLevel = task.Level
			wgi.Add(1)
			go doTask(task, wgi)
		}
	}
	wgi.Wait()
}

// doTask executes a single task and reports its completion.
func doTask(task *domain.Task, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Printf("Task %s started\n", task.Label)
	task.Run()
	fmt.Printf("Task %s done\n", task.Label)
}

// ExportToFile exports the Oasis, its dependency graph, and task tree to files.
func (svc BuilderService) ExportToFile(oasis domain.Oasis, dependencyGraph domain.DAG, taskTree domain.TaskTree) {
	svc.fsp.ExportOasisTaskTreeToFile(&oasis, &taskTree)
	svc.fsp.ExportOasisDependencyToFile(&oasis, &dependencyGraph)
}
