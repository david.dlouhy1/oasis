// Package service provides implementations of various services.
package service

import (
	"reflect"
	"testing"

	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
)

func TestNewPlanService(t *testing.T) {
	type args struct {
		pp ports.PlanPort
	}
	tests := []struct {
		name string
		args args
		want *PlanService
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewPlanService(tt.args.pp); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPlanService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPlanService_CreatePlan(t *testing.T) {
	type fields struct {
		pp ports.PlanPort
	}
	type args struct {
		name string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantPlan *domain.Plan
		wantErr  bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := PlanService{
				pp: tt.fields.pp,
			}
			gotPlan, err := svc.CreatePlan(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("PlanService.CreatePlan() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotPlan, tt.wantPlan) {
				t.Errorf("PlanService.CreatePlan() = %v, want %v", gotPlan, tt.wantPlan)
			}
		})
	}
}
