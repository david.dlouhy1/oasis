// Package service provides implementations of various services.
package service

import (
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
)

// PlanService implements the service for handling plans.
type PlanService struct {
	pp ports.PlanPort
}

// NewPlanService creates a new instance of PlanService.
func NewPlanService(pp ports.PlanPort) *PlanService {
	return &PlanService{
		pp: pp,
	}
}

// CreatePlan creates a new plan.
func (svc PlanService) CreatePlan(name string) (plan *domain.Plan, err error) {
	plan, err = svc.pp.CreatePlan(name)
	return
}
