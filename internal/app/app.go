// Package app provides the main application structure and initialization logic.
package app

import (
	consoleAdapter "gitlab.com/david.dlouhy1/oasis/internal/adapters/driven/console"
	fsAdapter "gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/fs"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
	service "gitlab.com/david.dlouhy1/oasis/internal/service/builder"
)

// App represents the main application structure.
type App struct {
	FileSystemPort ports.FSPort         // FileSystemPort represents the port for interacting with the filesystem.
	BuilderService ports.BuilderService // BuilderService represents the service for building processes.
	ConsolePort    ports.ConsolePort    // ConsolePort represents the port for handling console commands.
}

// NewApp creates a new instance of the application with initialized ports and services.
func NewApp() *App {
	fsa := fsAdapter.NewAdapter()
	bs := service.NewBuilderService(fsa)
	ca := consoleAdapter.NewAdapter(bs)
	return &App{
		FileSystemPort: fsa,
		BuilderService: bs,
		ConsolePort:    ca,
	}
}
