// Package app provides the main application structure and initialization logic.
package app

import (
	"reflect"
	"testing"
)

func TestNewApp(t *testing.T) {
	tests := []struct {
		name string
		want *App
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewApp(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewApp() = %v, want %v", got, tt.want)
			}
		})
	}
}
