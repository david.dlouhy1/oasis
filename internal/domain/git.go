// Package domain defines domain models and types used within the application.
package domain

// GitProject represents a Git project with UUID, owner, and name.
type GitProject struct {
	UUID  string // UUID uniquely identifying the Git project.
	Owner string // Owner of the Git project.
	Name  string // Name of the Git project.
}
