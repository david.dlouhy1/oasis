// Package domain defines domain models and types used within the application.
package domain

// DAG represents a Directed Acyclic Graph.
type DAG struct {
	Vertexes []*DAGVertex // Vertexes in the DAG.
	Edges    []*DAGEdge   // Edges in the DAG.
}

// DAGVertex represents a vertex in the Directed Acyclic Graph.
type DAGVertex struct {
	Value interface{} // Value associated with the vertex.
}

// DAGEdge represents a directed edge in the Directed Acyclic Graph.
type DAGEdge struct {
	Source      *DAGVertex // Source vertex of the edge.
	Destination *DAGVertex // Destination vertex of the edge.
	Type        string     // Type of the edge.
}

// NewDAG initializes a new Directed Acyclic Graph.
func NewDAG() (dag *DAG) {
	dag = &DAG{
		Vertexes: make([]*DAGVertex, 0),
		Edges:    make([]*DAGEdge, 0),
	}
	return
}

// AddVertex adds a vertex to the Directed Acyclic Graph.
func (dag *DAG) AddVertex(value interface{}) *DAGVertex {
	vertex := &DAGVertex{Value: value}
	dag.Vertexes = append(dag.Vertexes, vertex)
	return vertex
}

// AddEdge adds a directed edge from a source vertex to a destination vertex in the Directed Acyclic Graph.
func (dag *DAG) AddEdge(source *DAGVertex, destination *DAGVertex, edgeType string) {
	edge := &DAGEdge{Source: source, Destination: destination, Type: edgeType}
	dag.Edges = append(dag.Edges, edge)
}

// FindRootNode finds all root nodes (nodes that have no parents) in the Directed Acyclic Graph.
func (dag *DAG) FindRootNode() (rootNodes []*DAGVertex) {
	childVertex := make(map[*DAGVertex]bool)
	for _, edge := range dag.Edges {
		childVertex[edge.Destination] = true
	}
	for _, vertex := range dag.Vertexes {
		if !childVertex[vertex] {
			rootNodes = append(rootNodes, vertex)
		}
	}
	return
}

// GetChildVertexes finds all child vertexes for a specific vertex in the Directed Acyclic Graph.
func (dag *DAG) GetChildVertexes(vertex *DAGVertex) (childVertexes []*DAGVertex) {
	for _, edge := range dag.Edges {
		if vertex == edge.Source {
			childVertexes = append(childVertexes, edge.Destination)
		}
	}
	return
}

// GetEdge finds the edge between two specified vertices in the Directed Acyclic Graph.
func (dag *DAG) GetEdge(source *DAGVertex, destination *DAGVertex) *DAGEdge {
	for _, edge := range dag.Edges {
		if source == edge.Source && destination == edge.Destination {
			return edge
		}
	}
	return nil
}
