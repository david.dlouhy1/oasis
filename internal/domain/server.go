// Package domain defines domain models and logic for the application.
package domain

// ProjectServerResource represents the server resources required for deploying and monitoring a project.
type ProjectServerResource struct {
	DeployFolder          string // DeployFolder is the folder path on the server where the project will be deployed.
	DeployHost            string // DeployHost is the hostname or IP address of the server where the project will be deployed.
	DeployPortTest        string // DeployPortTest is the port number for testing deployment on the server.
	DeployPortStaging     string // DeployPortStaging is the port number for staging deployment on the server.
	DeployUser            string // DeployUser is the username used for SSH authentication when deploying the project.
	MonitoringPortTest    string // MonitoringPortTest is the port number for testing monitoring services.
	MonitoringPortStaging string // MonitoringPortStaging is the port number for staging monitoring services.
	SSHKey                string // SSHKey is the SSH private key used for authentication when accessing the server.
	AuthorizedKeys        string // AuthorizedKeys is a list of SSH public keys authorized to access the server.
}
