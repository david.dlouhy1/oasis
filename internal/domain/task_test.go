// Package domain defines domain models and logic for the application.
package domain

import (
	"reflect"
	"testing"
)

func TestNewTask(t *testing.T) {
	type args struct {
		label   string
		handler func()
	}
	tests := []struct {
		name string
		args args
		want *Task
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewTask(tt.args.label, tt.args.handler); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewTask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewLink(t *testing.T) {
	type args struct {
		from *Task
		to   *Task
	}
	tests := []struct {
		name string
		args args
		want *Link
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewLink(tt.args.from, tt.args.to); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewLink() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewTaskTree(t *testing.T) {
	tests := []struct {
		name         string
		wantTaskTree *TaskTree
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotTaskTree := NewTaskTree(); !reflect.DeepEqual(gotTaskTree, tt.wantTaskTree) {
				t.Errorf("NewTaskTree() = %v, want %v", gotTaskTree, tt.wantTaskTree)
			}
		})
	}
}

func TestTaskTree_AddTask(t *testing.T) {
	type fields struct {
		Tasks []*Task
		Links []*Link
	}
	type args struct {
		task *Task
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			taskTree := &TaskTree{
				Tasks: tt.fields.Tasks,
				Links: tt.fields.Links,
			}
			taskTree.AddTask(tt.args.task)
		})
	}
}

func TestTaskTree_RemoveTask(t *testing.T) {
	type fields struct {
		Tasks []*Task
		Links []*Link
	}
	type args struct {
		task *Task
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			taskTree := &TaskTree{
				Tasks: tt.fields.Tasks,
				Links: tt.fields.Links,
			}
			taskTree.RemoveTask(tt.args.task)
		})
	}
}

func TestTaskTree_RemoveLink(t *testing.T) {
	type fields struct {
		Tasks []*Task
		Links []*Link
	}
	type args struct {
		link *Link
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			taskTree := &TaskTree{
				Tasks: tt.fields.Tasks,
				Links: tt.fields.Links,
			}
			taskTree.RemoveLink(tt.args.link)
		})
	}
}

func TestTaskTree_GetChildLinks(t *testing.T) {
	type fields struct {
		Tasks []*Task
		Links []*Link
	}
	type args struct {
		task *Task
	}
	tests := []struct {
		name           string
		fields         fields
		args           args
		wantChildLinks []*Link
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			taskTree := &TaskTree{
				Tasks: tt.fields.Tasks,
				Links: tt.fields.Links,
			}
			if gotChildLinks := taskTree.GetChildLinks(tt.args.task); !reflect.DeepEqual(gotChildLinks, tt.wantChildLinks) {
				t.Errorf("TaskTree.GetChildLinks() = %v, want %v", gotChildLinks, tt.wantChildLinks)
			}
		})
	}
}

func TestTaskTree_AddLink(t *testing.T) {
	type fields struct {
		Tasks []*Task
		Links []*Link
	}
	type args struct {
		link *Link
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			taskTree := &TaskTree{
				Tasks: tt.fields.Tasks,
				Links: tt.fields.Links,
			}
			taskTree.AddLink(tt.args.link)
		})
	}
}

func TestTaskTree_GetTasksCount(t *testing.T) {
	type fields struct {
		Tasks []*Task
		Links []*Link
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			taskTree := &TaskTree{
				Tasks: tt.fields.Tasks,
				Links: tt.fields.Links,
			}
			if got := taskTree.GetTasksCount(); got != tt.want {
				t.Errorf("TaskTree.GetTasksCount() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTaskTree_GetNumbersOfParents(t *testing.T) {
	type fields struct {
		Tasks []*Task
		Links []*Link
	}
	tests := []struct {
		name                 string
		fields               fields
		wantNumbersOfParents map[*Task]int
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			taskTree := &TaskTree{
				Tasks: tt.fields.Tasks,
				Links: tt.fields.Links,
			}
			if gotNumbersOfParents := taskTree.GetNumbersOfParents(); !reflect.DeepEqual(gotNumbersOfParents, tt.wantNumbersOfParents) {
				t.Errorf("TaskTree.GetNumbersOfParents() = %v, want %v", gotNumbersOfParents, tt.wantNumbersOfParents)
			}
		})
	}
}

func TestNewTaskGroup(t *testing.T) {
	tests := []struct {
		name          string
		wantTaskGroup *TaskGroup
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotTaskGroup := NewTaskGroup(); !reflect.DeepEqual(gotTaskGroup, tt.wantTaskGroup) {
				t.Errorf("NewTaskGroup() = %v, want %v", gotTaskGroup, tt.wantTaskGroup)
			}
		})
	}
}

func TestNewBuildQueue(t *testing.T) {
	tests := []struct {
		name           string
		wantBuildQueue *BuildQueue
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotBuildQueue := NewBuildQueue(); !reflect.DeepEqual(gotBuildQueue, tt.wantBuildQueue) {
				t.Errorf("NewBuildQueue() = %v, want %v", gotBuildQueue, tt.wantBuildQueue)
			}
		})
	}
}

func TestBuildQueue_AddTask(t *testing.T) {
	type fields struct {
		TaskGroups []*TaskGroup
	}
	type args struct {
		task  *Task
		level int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buildQueue := &BuildQueue{
				TaskGroups: tt.fields.TaskGroups,
			}
			buildQueue.AddTask(tt.args.task, tt.args.level)
		})
	}
}

func TestTask_Run(t *testing.T) {
	type fields struct {
		Label   string
		Handler func()
		Level   int
	}
	tests := []struct {
		name   string
		fields fields
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			task := &Task{
				Label:   tt.fields.Label,
				Handler: tt.fields.Handler,
				Level:   tt.fields.Level,
			}
			task.Run()
		})
	}
}
