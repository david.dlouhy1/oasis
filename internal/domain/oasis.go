// Package domain defines domain models and types used within the application.
package domain

// Constants representing project types and IDs.
const (
	// PROJECT_TYPE_EMBEDDED_OS_LINUX represents the type of embedded OS project as Linux.
	PROJECT_TYPE_EMBEDDED_OS_LINUX = "Embedded OS (Linux)"
	// PROJECT_FORK_ID_EMBEDDED_OS_LINUX represents the fork ID for the embedded OS project with Linux.
	PROJECT_FORK_ID_EMBEDDED_OS_LINUX = 56953737
)

// Oasis represents a collection of projects and associated technologies.
type Oasis struct {
	Name         string       `yaml:"name"`         // Name of the Oasis.
	Description  string       `yaml:"description"`  // Description of the Oasis.
	Technologies Technologies `yaml:"technologies"` // List of technologies used in the Oasis.
	Projects     Projects     `yaml:"projects"`     // List of projects in the Oasis.
}

// Technologies represents a collection of technologies.
type Technologies struct {
	Technologies []Technology `yaml:"technologies"` // List of individual technologies.
}

// Projects represents a collection of projects.
type Projects struct {
	Projects []Project `yaml:"projects"` // List of individual projects.
}

// Technology represents a specific technology.
type Technology struct {
	Name       string                 `yaml:"name"`       // Name of the technology.
	Type       []string               `yaml:"type"`       // Type of technology.
	Definition map[string]interface{} `yaml:"definition"` // Additional definition or properties of the technology.
}

// Project represents a specific project.
type Project struct {
	Name    string      `yaml:"name"`    // Name of the project.
	Type    ProjectType `yaml:"type"`    // Type of the project.
	VCS     string      `yaml:"vcs"`     // Version control system used for the project.
	Infra   string      `yaml:"infra"`   // Infrastructure associated with the project.
	Plan    string      `yaml:"plan"`    // Plan or strategy associated with the project.
	Monitor string      `yaml:"monitor"` // Monitoring setup associated with the project.
}

// ProjectType represents the type of a project.
type ProjectType string
