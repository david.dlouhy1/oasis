// Package domain defines domain models and logic for the application.
package domain

// Runner represents a continuous integration (CI) runner used for executing project builds.
type Runner struct {
	Project     GitProject // Project is the Git project associated with the runner.
	Name        string     // Name is the name of the CI runner.
	Host        string     // Host is the hostname or IP address of the CI runner's server.
	Token       string     // Token is the authentication token used to register the CI runner.
	Description string     // Description is a brief description or identifier for the CI runner.
}
