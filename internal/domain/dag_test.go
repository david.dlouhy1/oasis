package domain

import (
	"testing"
)

func TestDAG(t *testing.T) {
	dag := NewDAG()
	vertexValue := "TestVertex"
	dag.AddVertex(vertexValue)
	if len(dag.Vertexes) != 1 {
		t.Error("Vertex not added to the DAG")
	}
	if dag.Vertexes[0].Value != vertexValue {
		t.Errorf("Expected vertex value %v, got %v", vertexValue, dag.Vertexes[0].Value)
	}
}

func TestAddVertex(t *testing.T) {
	dag := NewDAG()

	vertexValue := "TestVertex"
	dag.AddVertex(vertexValue)

	if len(dag.Vertexes) != 1 {
		t.Error("Vertex not added to the DAG")
	}
	if dag.Vertexes[0].Value != vertexValue {
		t.Errorf("Expected vertex value %v, got %v", vertexValue, dag.Vertexes[0].Value)
	}
}

func TestAddEdge(t *testing.T) {
	dag := NewDAG()

	sourceVertex := dag.AddVertex("Source")
	destinationVertex := dag.AddVertex("Destination")
	edgeType := "Type"
	dag.AddEdge(sourceVertex, destinationVertex, edgeType)

	if len(dag.Edges) != 1 {
		t.Error("Edge not added to the DAG")
	}

	edge := dag.Edges[0]
	if edge.Source != sourceVertex || edge.Destination != destinationVertex || edge.Type != edgeType {
		t.Error("Edge not added correctly")
	}
}

func TestFindRootNode(t *testing.T) {
	dag := NewDAG()

	sourceVertex := dag.AddVertex("Source")
	destinationVertex := dag.AddVertex("Destination")
	dag.AddEdge(sourceVertex, destinationVertex, "Type")

	rootNodes := dag.FindRootNode()

	if len(rootNodes) != 1 || rootNodes[0] != sourceVertex {
		t.Error("Root node not found correctly")
	}
}

func TestGetChildVertexes(t *testing.T) {
	dag := NewDAG()

	sourceVertex := dag.AddVertex("Source")
	destinationVertex := dag.AddVertex("Destination")
	dag.AddEdge(sourceVertex, destinationVertex, "Type")

	childVertexes := dag.GetChildVertexes(sourceVertex)

	if len(childVertexes) != 1 || childVertexes[0] != destinationVertex {
		t.Error("Child vertexes not retrieved correctly")
	}
}

func TestGetEdge(t *testing.T) {
	dag := NewDAG()

	sourceVertex := dag.AddVertex("Source")
	destinationVertex := dag.AddVertex("Destination")
	edgeType := "Type"
	dag.AddEdge(sourceVertex, destinationVertex, edgeType)

	foundEdge := dag.GetEdge(sourceVertex, destinationVertex)

	if foundEdge == nil || foundEdge.Source != sourceVertex || foundEdge.Destination != destinationVertex || foundEdge.Type != edgeType {
		t.Error("Edge not retrieved correctly")
	}
}
