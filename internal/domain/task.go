// Package domain defines domain models and logic for the application.
package domain

// Task represents a task to be executed.
type Task struct {
	Label   string // Label is the name or description of the task.
	Handler func() // Handler is the function to be executed when the task runs.
	Level   int    // Level represents the hierarchical level of the task.
}

// Link represents a directed link between two tasks.
type Link struct {
	From *Task // From represents the source task of the link.
	To   *Task // To represents the target task of the link.
}

// NewTask creates a new Task with the given label and handler function.
func NewTask(label string, handler func()) *Task {
	return &Task{
		Label:   label,
		Handler: handler,
	}
}

// NewLink creates a new Link between the given source and target tasks.
func NewLink(from *Task, to *Task) *Link {
	return &Link{
		From: from,
		To:   to,
	}
}

// TaskTree represents a tree structure of tasks and their relationships.
type TaskTree struct {
	Tasks []*Task // Tasks represents the list of tasks in the task tree.
	Links []*Link // Links represents the list of links between tasks in the task tree.
}

// TaskGroup represents a group of related tasks.
type TaskGroup struct {
	Tasks []*Task // Tasks represents the list of tasks in the group.
}

// BuildQueue represents a queue of tasks to be executed.
type BuildQueue struct {
	TaskGroups []*TaskGroup // TaskGroups represents the list of task groups in the build queue.
}

// NewTaskTree creates a new TaskTree instance.
func NewTaskTree() (taskTree *TaskTree) {
	taskTree = &TaskTree{
		Tasks: make([]*Task, 0),
		Links: make([]*Link, 0),
	}
	return
}

// AddTask adds a task to the task tree.
func (taskTree *TaskTree) AddTask(task *Task) {
	taskTree.Tasks = append(taskTree.Tasks, task)
}

// RemoveTask removes a task from the task tree.
func (taskTree *TaskTree) RemoveTask(task *Task) {
	for i, tt := range taskTree.Tasks {
		if tt == task {
			taskTree.Tasks = append(taskTree.Tasks[:i], taskTree.Tasks[i+1:]...)
		}
	}
}

// RemoveLink removes a link from the task tree.
func (taskTree *TaskTree) RemoveLink(link *Link) {
	for i, tl := range taskTree.Links {
		if tl == link {
			taskTree.Links = append(taskTree.Links[:i], taskTree.Links[i+1:]...)
		}
	}
}

// GetChildLinks returns the links whose source is the given task.
func (taskTree *TaskTree) GetChildLinks(task *Task) (childLinks []*Link) {
	childLinks = []*Link{}
	for _, link := range taskTree.Links {
		if link.From == task {
			childLinks = append(childLinks, link)
		}
	}
	return
}

// AddLink adds a link to the task tree.
func (taskTree *TaskTree) AddLink(link *Link) {
	taskTree.Links = append(taskTree.Links, link)
}

// GetTasksCount returns the number of tasks in the task tree.
func (taskTree *TaskTree) GetTasksCount() int {
	return len(taskTree.Tasks)
}

// GetNumbersOfParents returns the number of parents for each task in the task tree.
func (taskTree *TaskTree) GetNumbersOfParents() (numbersOfParents map[*Task]int) {
	numbersOfParents = make(map[*Task]int)
	for _, task := range taskTree.Tasks {
		numbersOfParents[task] = 0
	}
	for _, link := range taskTree.Links {
		numbersOfParents[link.To]++
	}
	return
}

// NewTaskGroup creates a new TaskGroup instance.
func NewTaskGroup() (taskGroup *TaskGroup) {
	taskGroup = &TaskGroup{
		Tasks: make([]*Task, 0),
	}
	return
}

// NewBuildQueue creates a new BuildQueue instance.
func NewBuildQueue() (buildQueue *BuildQueue) {
	buildQueue = &BuildQueue{}
	return
}

// AddTask adds a task to the build queue with the specified level.
func (buildQueue *BuildQueue) AddTask(task *Task, level int) {
	tg := buildQueue.TaskGroups[level]
	if tg == nil {
		tg = NewTaskGroup()
		buildQueue.TaskGroups[level] = tg
	}
	tg.Tasks = append(tg.Tasks, task)
}

func (task *Task) Run() {
	task.Handler()
}
