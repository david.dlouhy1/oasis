// Package console provides an adapter for handling console commands and interactions.
// It utilizes the Cobra library for command-line interface implementation.
package console

import (
	"fmt"
	"log"
	"sync"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
)

// Adapter represents the console adapter.
type Adapter struct {
	bs ports.BuilderService // Builder service for generating build process steps.
}

// NewAdapter creates a new console adapter with the provided BuilderService.
func NewAdapter(bs ports.BuilderService) *Adapter {
	return &Adapter{
		bs: bs,
	}
}

// HandleApply handles the "apply" command and initiates the build process.
func (adapter *Adapter) HandleApply(cmd *cobra.Command, args []string) {
	fmt.Println("Generating steps...")

	path, err := getPathFromFlag(cmd.Flags())
	handleError(err)

	fmt.Printf("Using path: %s\n", path)

	oasis := adapter.bs.LoadOasis(path)
	dependencyGraph := adapter.bs.GenerateDependencyGraph(oasis)
	taskTree := adapter.bs.GenerateTaskTree(oasis, dependencyGraph)
	wg := &sync.WaitGroup{}
	wg.Add(3)
	go printInfoGenerate(*oasis, *dependencyGraph, *taskTree, wg)
	go adapter.ExportBuildProcessToFile(*oasis, *dependencyGraph, *taskTree, wg)
	go adapter.HandleBuildProcess(copyTaskTree(taskTree), wg)
	wg.Wait()
}

// getPathFromFlag retrieves the file path from the command-line flags.
func getPathFromFlag(fs *pflag.FlagSet) (path string, err error) {
	path, err = fs.GetString("file")
	return
}

// handleError handles errors by logging them.
func handleError(err error) {
	if err != nil {
		log.Println(err)
		return
	}
}

// printInfoGenerate prints information about the loaded Oasis, dependency graph, and task tree.
func printInfoGenerate(oasis domain.Oasis, dependencyGraph domain.DAG, taskTree domain.TaskTree, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Printf("Loaded Oasis:\n%+v\n", oasis)
	fmt.Printf("Loaded Dependencies:\n%+v\n", dependencyGraph)
	fmt.Printf("Loaded Task tree:\n%+v\n", taskTree)
}

// HandleBuildProcess handles the build process for the loaded task tree.
func (adapter Adapter) HandleBuildProcess(taskTree *domain.TaskTree, wg *sync.WaitGroup) {
	defer wg.Done()
	adapter.bs.BuildOasis(taskTree)
}

// ExportBuildProcessToFile exports the build process details to a file.
func (adapter Adapter) ExportBuildProcessToFile(oasis domain.Oasis, dependencyGraph domain.DAG, taskTree domain.TaskTree, wg *sync.WaitGroup) {
	defer wg.Done()
	adapter.bs.ExportToFile(oasis, dependencyGraph, taskTree)
}

// copyTaskTree creates a deep copy of the task tree to avoid mutation of the original tree.
func copyTaskTree(input *domain.TaskTree) (output *domain.TaskTree) {
	if input == nil {
		return nil
	}
	output = &domain.TaskTree{}
	taskCopies := make(map[*domain.Task]*domain.Task)
	for _, task := range input.Tasks {
		taskCopy := &domain.Task{
			Label:   task.Label,
			Handler: task.Handler,
		}
		taskCopies[task] = taskCopy
		output.Tasks = append(output.Tasks, taskCopy)
	}
	for _, link := range input.Links {
		fromCopy := taskCopies[link.From]
		toCopy := taskCopies[link.To]
		if fromCopy != nil && toCopy != nil {
			output.Links = append(output.Links, &domain.Link{
				From: fromCopy,
				To:   toCopy,
			})
		}
	}

	return output
}
