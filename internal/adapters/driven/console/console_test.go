// Package console provides an adapter for handling console commands and interactions.
// It utilizes the Cobra library for command-line interface implementation.
package console

import (
	"reflect"
	"sync"
	"testing"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"gitlab.com/david.dlouhy1/oasis/internal/ports"
)

func TestNewAdapter(t *testing.T) {
	type args struct {
		bs ports.BuilderService
	}
	tests := []struct {
		name string
		args args
		want *Adapter
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAdapter(tt.args.bs); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAdapter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdapter_HandleApply(t *testing.T) {
	type fields struct {
		bs ports.BuilderService
	}
	type args struct {
		cmd  *cobra.Command
		args []string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				bs: tt.fields.bs,
			}
			adapter.HandleApply(tt.args.cmd, tt.args.args)
		})
	}
}

func Test_getPathFromFlag(t *testing.T) {
	type args struct {
		fs *pflag.FlagSet
	}
	tests := []struct {
		name     string
		args     args
		wantPath string
		wantErr  bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPath, err := getPathFromFlag(tt.args.fs)
			if (err != nil) != tt.wantErr {
				t.Errorf("getPathFromFlag() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotPath != tt.wantPath {
				t.Errorf("getPathFromFlag() = %v, want %v", gotPath, tt.wantPath)
			}
		})
	}
}

func Test_printInfoGenerate(t *testing.T) {
	type args struct {
		oasis           domain.Oasis
		dependencyGraph domain.DAG
		taskTree        domain.TaskTree
		wg              *sync.WaitGroup
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			printInfoGenerate(tt.args.oasis, tt.args.dependencyGraph, tt.args.taskTree, tt.args.wg)
		})
	}
}

func TestAdapter_HandleBuildProcess(t *testing.T) {
	type fields struct {
		bs ports.BuilderService
	}
	type args struct {
		taskTree *domain.TaskTree
		wg       *sync.WaitGroup
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := Adapter{
				bs: tt.fields.bs,
			}
			adapter.HandleBuildProcess(tt.args.taskTree, tt.args.wg)
		})
	}
}

func TestAdapter_ExportBuildProcessToFile(t *testing.T) {
	type fields struct {
		bs ports.BuilderService
	}
	type args struct {
		oasis           domain.Oasis
		dependencyGraph domain.DAG
		taskTree        domain.TaskTree
		wg              *sync.WaitGroup
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := Adapter{
				bs: tt.fields.bs,
			}
			adapter.ExportBuildProcessToFile(tt.args.oasis, tt.args.dependencyGraph, tt.args.taskTree, tt.args.wg)
		})
	}
}

func Test_copyTaskTree(t *testing.T) {
	type args struct {
		input *domain.TaskTree
	}
	tests := []struct {
		name       string
		args       args
		wantOutput *domain.TaskTree
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotOutput := copyTaskTree(tt.args.input); !reflect.DeepEqual(gotOutput, tt.wantOutput) {
				t.Errorf("copyTaskTree() = %v, want %v", gotOutput, tt.wantOutput)
			}
		})
	}
}
