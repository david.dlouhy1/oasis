// Package fs provides an adapter for interacting with the file system.
// It offers functionalities such as loading files, generating SSH key pairs,
// and exporting data to files.
package fs

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"sync"

	"golang.org/x/crypto/ssh"

	"github.com/go-yaml/yaml"
	"github.com/goccy/go-graphviz"
	"github.com/goccy/go-graphviz/cgraph"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

// Adapter represents the file system adapter.
type Adapter struct {
	dataFilePath       string // Path to the data file.
	privateKeyFilePath string // Path to the private key file.
	publicKeyFilePath  string // Path to the public key file.
}

// NewAdapter creates a new file system adapter.
func NewAdapter() *Adapter {
	dataFilePath := "data"
	return &Adapter{
		dataFilePath:       dataFilePath,
		privateKeyFilePath: dataFilePath + "/id_rsa",
		publicKeyFilePath:  dataFilePath + "/id_rsa.pub",
	}
}

// Oasis represents the structure of an Oasis file.
type Oasis struct {
	Oasis domain.Oasis `yaml:"oasis"`
}

// GetOasisFiles recursively retrieves Oasis files from the specified directory.
func (adapter Adapter) GetOasisFiles(dirPath string, fileChan chan<- string, errorChan chan error, wg *sync.WaitGroup) {
	defer wg.Done()
	err := filepath.WalkDir(dirPath, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		fileInfo, err := d.Info()
		if err != nil {
			return err
		}
		if !fileInfo.IsDir() {
			fileChan <- path
		}
		return nil
	})
	if err != nil {
		errorChan <- err
		return
	}

	subDirs, err := filepath.Glob(filepath.Join(dirPath, "*"))
	if err != nil {
		errorChan <- err
		return
	}
	for _, subDir := range subDirs {
		if fileInfo, err := os.Stat(subDir); err == nil && fileInfo.IsDir() {
			wg.Add(1)
			go adapter.GetOasisFiles(subDir, fileChan, errorChan, wg)
		}
	}
}

// LoadOasisFiles loads Oasis files into the provided Oasis struct.
func (adapter Adapter) LoadOasisFiles(oasis *domain.Oasis, fileChan <-chan string, errorChan chan error) {
	processed := make(map[string]bool)
	for file := range fileChan {
		if _, ok := processed[file]; !ok {
			processed[file] = true
			if err := loadOasisFile(file, oasis); err != nil {
				errorChan <- err
			}
		}
	}
}

// Loads Oasis files
func loadOasisFile(path string, oasis *domain.Oasis) (err error) {
	yamlContent, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	replaceWithEnvVariable(&yamlContent)

	yamlMap := make(map[string]interface{})
	if err := yaml.Unmarshal(yamlContent, yamlMap); err != nil {
		return err
	}
	var key string

	for k := range yamlMap {
		key = k
		break
	}

	switch key {
	case "oasis":
		fmt.Printf("PF: %s\n", path)
		to := &Oasis{}
		if oasis.Name != "" {
			return errors.New("duplicated oasis definition")
		}
		if err := yaml.Unmarshal(yamlContent, to); err != nil {
			return err
		}
		*oasis = to.Oasis

	case "technologies":
		fmt.Printf("TF: %s\n", path)
		technologies := &domain.Technologies{}
		if err := yaml.Unmarshal(yamlContent, technologies); err != nil {
			return err
		}
		oasis.Technologies.Technologies = append(oasis.Technologies.Technologies, technologies.Technologies...)

	case "projects":
		fmt.Printf("PF: %s\n", path)
		projects := &domain.Projects{}
		if err := yaml.Unmarshal(yamlContent, projects); err != nil {
			return err
		}
		oasis.Projects.Projects = append(oasis.Projects.Projects, projects.Projects...)
	default:
		return errors.New("not oasis file")
	}
	return
}

// replace key content of file with value
func replaceWithEnvVariable(bytes *[]byte) {
	re := regexp.MustCompile(`\$OASIS_\w+`)
	replacedContent := re.ReplaceAllStringFunc(string(*bytes), func(match string) string {
		key := match[1:]
		value := os.Getenv(key)
		if value == "" {
			return match
		}
		return value
	})
	*bytes = []byte(replacedContent)
}

// ExportOasisDependencyToFile exports the Oasis dependency graph to a file.
func (adapter Adapter) ExportOasisDependencyToFile(oasis *domain.Oasis, dependencyGraph *domain.DAG) {
	g := graphviz.New()
	graph, err := g.Graph()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := graph.Close(); err != nil {
			log.Fatal(err)
		}
		g.Close()
	}()
	graphText := fmt.Sprintf("\n\n\n%s\n%s", oasis.Name, oasis.Description)
	graph.SetLabel(graphText)

	nodes := make(map[*domain.DAGVertex]*cgraph.Node)

	for _, vertex := range dependencyGraph.Vertexes {

		var nodeName string
		switch vertex.Value.(type) {
		case domain.Project:
			nodeName = vertex.Value.(domain.Project).Name
		case domain.Technology:
			nodeName = vertex.Value.(domain.Technology).Name
		default:
			nodeName = "error"
		}
		node, err := graph.CreateNode(nodeName)
		if err != nil {
			log.Fatal(err)
		}
		nodes[vertex] = node
	}

	for i, edge := range dependencyGraph.Edges {
		e, err := graph.CreateEdge(strconv.Itoa(i), nodes[edge.Source], nodes[edge.Destination])
		if err != nil {
			log.Fatal(err)
		}
		e.SetDir(cgraph.BackDir)
		e.SetLabel(edge.Type)
	}

	if err := g.RenderFilename(graph, graphviz.SVG, "oasis.svg"); err != nil {
		log.Fatal(err)
	}
}

// ExportOasisTaskTreeToFile exports the Oasis task tree to a file.
func (adapter Adapter) ExportOasisTaskTreeToFile(oasis *domain.Oasis, taskTree *domain.TaskTree) {
	g := graphviz.New()
	graph, err := g.Graph()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := graph.Close(); err != nil {
			log.Fatal(err)
		}
		g.Close()
	}()
	graphText := fmt.Sprintf("\n\n\nTask tree for %s", oasis.Name)
	graph.SetLabel(graphText)

	nodes := make(map[*domain.Task]*cgraph.Node)

	for _, task := range taskTree.Tasks {

		node, err := graph.CreateNode(task.Label)
		if err != nil {
			log.Fatal(err)
		}
		nodes[task] = node
	}

	for i, link := range taskTree.Links {
		e, err := graph.CreateEdge(strconv.Itoa(i), nodes[link.From], nodes[link.To])
		if err != nil {
			log.Fatal(err)
		}
		e.SetDir(cgraph.ForwardDir)
	}

	if err := g.RenderFilename(graph, graphviz.SVG, "tt.svg"); err != nil {
		log.Fatal(err)
	}
}

// GenerateSSHKeyPair generates a new SSH key pair and saves it to files.
func (adapter *Adapter) GenerateSSHKeyPair() (err error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return
	}
	publicKey, err := ssh.NewPublicKey(privateKey.Public())
	if err != nil {
		return
	}

	privateKeyStr := marshalRSAPrivate(privateKey)
	publicKeyStr := string(ssh.MarshalAuthorizedKey(publicKey))

	if !fileExists(adapter.privateKeyFilePath) {
		err = os.WriteFile(adapter.privateKeyFilePath, []byte(privateKeyStr), 0600)
		if err != nil {
			return
		}
	}

	if !fileExists(adapter.publicKeyFilePath) {
		err = os.WriteFile(adapter.publicKeyFilePath, []byte(publicKeyStr), 0644)
		if err != nil {
			return
		}
	}
	return
}

// GetPrivateSSHKey retrieves the private SSH key from the file system.
func (adapter *Adapter) GetPrivateSSHKey() (privateKey string, err error) {
	privateKeyByte, err := os.ReadFile(adapter.privateKeyFilePath)
	privateKey = string(privateKeyByte)
	return
}

// GetPublicSSHKey retrieves the public SSH key from the file system.
func (adapter *Adapter) GetPublicSSHKey() (publicKey string, err error) {
	publicKeyByte, err := os.ReadFile(adapter.publicKeyFilePath)
	publicKey = string(publicKeyByte)
	return
}

// Marshals private key
func marshalRSAPrivate(priv *rsa.PrivateKey) string {
	return string(pem.EncodeToMemory(&pem.Block{
		Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv),
	}))
}

// Tests if file exists
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
