// Package fs provides an adapter for interacting with the file system.
// It offers functionalities such as loading files, generating SSH key pairs,
// and exporting data to files.
package fs

import (
	"crypto/rsa"
	"reflect"
	"sync"
	"testing"

	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

func TestNewAdapter(t *testing.T) {
	tests := []struct {
		name string
		want *Adapter
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAdapter(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAdapter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdapter_GetOasisFiles(t *testing.T) {
	type fields struct {
		dataFilePath       string
		privateKeyFilePath string
		publicKeyFilePath  string
	}
	type args struct {
		dirPath   string
		fileChan  chan<- string
		errorChan chan error
		wg        *sync.WaitGroup
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := Adapter{
				dataFilePath:       tt.fields.dataFilePath,
				privateKeyFilePath: tt.fields.privateKeyFilePath,
				publicKeyFilePath:  tt.fields.publicKeyFilePath,
			}
			adapter.GetOasisFiles(tt.args.dirPath, tt.args.fileChan, tt.args.errorChan, tt.args.wg)
		})
	}
}

func TestAdapter_LoadOasisFiles(t *testing.T) {
	type fields struct {
		dataFilePath       string
		privateKeyFilePath string
		publicKeyFilePath  string
	}
	type args struct {
		oasis     *domain.Oasis
		fileChan  <-chan string
		errorChan chan error
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := Adapter{
				dataFilePath:       tt.fields.dataFilePath,
				privateKeyFilePath: tt.fields.privateKeyFilePath,
				publicKeyFilePath:  tt.fields.publicKeyFilePath,
			}
			adapter.LoadOasisFiles(tt.args.oasis, tt.args.fileChan, tt.args.errorChan)
		})
	}
}

func Test_loadOasisFile(t *testing.T) {
	type args struct {
		path  string
		oasis *domain.Oasis
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := loadOasisFile(tt.args.path, tt.args.oasis); (err != nil) != tt.wantErr {
				t.Errorf("loadOasisFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_replaceWithEnvVariable(t *testing.T) {
	type args struct {
		bytes *[]byte
	}
	tests := []struct {
		name string
		args args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			replaceWithEnvVariable(tt.args.bytes)
		})
	}
}

func TestAdapter_ExportOasisDependencyToFile(t *testing.T) {
	type fields struct {
		dataFilePath       string
		privateKeyFilePath string
		publicKeyFilePath  string
	}
	type args struct {
		oasis           *domain.Oasis
		dependencyGraph *domain.DAG
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := Adapter{
				dataFilePath:       tt.fields.dataFilePath,
				privateKeyFilePath: tt.fields.privateKeyFilePath,
				publicKeyFilePath:  tt.fields.publicKeyFilePath,
			}
			adapter.ExportOasisDependencyToFile(tt.args.oasis, tt.args.dependencyGraph)
		})
	}
}

func TestAdapter_ExportOasisTaskTreeToFile(t *testing.T) {
	type fields struct {
		dataFilePath       string
		privateKeyFilePath string
		publicKeyFilePath  string
	}
	type args struct {
		oasis    *domain.Oasis
		taskTree *domain.TaskTree
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := Adapter{
				dataFilePath:       tt.fields.dataFilePath,
				privateKeyFilePath: tt.fields.privateKeyFilePath,
				publicKeyFilePath:  tt.fields.publicKeyFilePath,
			}
			adapter.ExportOasisTaskTreeToFile(tt.args.oasis, tt.args.taskTree)
		})
	}
}

func TestAdapter_GenerateSSHKeyPair(t *testing.T) {
	type fields struct {
		dataFilePath       string
		privateKeyFilePath string
		publicKeyFilePath  string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				dataFilePath:       tt.fields.dataFilePath,
				privateKeyFilePath: tt.fields.privateKeyFilePath,
				publicKeyFilePath:  tt.fields.publicKeyFilePath,
			}
			if err := adapter.GenerateSSHKeyPair(); (err != nil) != tt.wantErr {
				t.Errorf("Adapter.GenerateSSHKeyPair() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAdapter_GetPrivateSSHKey(t *testing.T) {
	type fields struct {
		dataFilePath       string
		privateKeyFilePath string
		publicKeyFilePath  string
	}
	tests := []struct {
		name           string
		fields         fields
		wantPrivateKey string
		wantErr        bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				dataFilePath:       tt.fields.dataFilePath,
				privateKeyFilePath: tt.fields.privateKeyFilePath,
				publicKeyFilePath:  tt.fields.publicKeyFilePath,
			}
			gotPrivateKey, err := adapter.GetPrivateSSHKey()
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.GetPrivateSSHKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotPrivateKey != tt.wantPrivateKey {
				t.Errorf("Adapter.GetPrivateSSHKey() = %v, want %v", gotPrivateKey, tt.wantPrivateKey)
			}
		})
	}
}

func TestAdapter_GetPublicSSHKey(t *testing.T) {
	type fields struct {
		dataFilePath       string
		privateKeyFilePath string
		publicKeyFilePath  string
	}
	tests := []struct {
		name          string
		fields        fields
		wantPublicKey string
		wantErr       bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				dataFilePath:       tt.fields.dataFilePath,
				privateKeyFilePath: tt.fields.privateKeyFilePath,
				publicKeyFilePath:  tt.fields.publicKeyFilePath,
			}
			gotPublicKey, err := adapter.GetPublicSSHKey()
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.GetPublicSSHKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotPublicKey != tt.wantPublicKey {
				t.Errorf("Adapter.GetPublicSSHKey() = %v, want %v", gotPublicKey, tt.wantPublicKey)
			}
		})
	}
}

func Test_marshalRSAPrivate(t *testing.T) {
	type args struct {
		priv *rsa.PrivateKey
	}
	tests := []struct {
		name string
		args args
		want string
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := marshalRSAPrivate(tt.args.priv); got != tt.want {
				t.Errorf("marshalRSAPrivate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_fileExists(t *testing.T) {
	type args struct {
		filename string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fileExists(tt.args.filename); got != tt.want {
				t.Errorf("fileExists() = %v, want %v", got, tt.want)
			}
		})
	}
}
