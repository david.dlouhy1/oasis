// Package github provides an adapter for interacting with GitHub.
// It facilitates operations such as creating, fetching, and deleting projects.
package github

import (
	"context"
	"strconv"
	"strings"

	"github.com/google/go-github/v57/github"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/vcp"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

// Adapter represents the GitHub adapter.
type Adapter struct {
	client *github.Client // GitHub client instance.
	user   *github.User   // User associated with the GitHub client.
}

// NewAdapter creates a new GitHub adapter with the provided access token.
func NewAdapter(token string) (*Adapter, error) {
	client := github.NewClient(nil).WithAuthToken(token)
	user, _, err := client.Users.Get(context.Background(), "")
	return &Adapter{
		client: client,
		user:   user,
	}, err
}

// CreateProject creates a new GitHub project with the specified name and type.
func (adapter *Adapter) CreateProject(name string, projectType domain.ProjectType) (project *domain.GitProject, err error) {
	createdProject, _, err := adapter.client.Repositories.Create(context.Background(), "", &github.Repository{Name: &name})
	if err != nil {
		switch parseError(&err).ErrorType {
		case vcp.ERROR_PROJECT_EXISTS:
			return nil, nil
		default:
			return nil, err
		}

	}
	project = &domain.GitProject{
		UUID:  strconv.FormatInt(*createdProject.ID, 10),
		Owner: *createdProject.Owner.Login,
		Name:  *createdProject.Name,
	}
	return
}

// GetProject retrieves information about a GitHub project by its name.
func (adapter *Adapter) GetProject(name string) (project *domain.GitProject, err error) {
	panic("not implemented")
}

// DeleteProject deletes the specified GitHub project.
func (adapter *Adapter) DeleteProject(project *domain.GitProject) error {
	_, err := adapter.client.Repositories.Delete(context.Background(), project.Owner, project.Name)
	return err
}

// parseError parses GitHub API errors and converts them into VCP errors.
func parseError(err *error) *vcp.VCPError {
	if strings.Contains((*err).Error(), "Message:name already exists on this account") {
		return vcp.NewVCPError(vcp.ERROR_PROJECT_EXISTS)
	}
	return nil
}
