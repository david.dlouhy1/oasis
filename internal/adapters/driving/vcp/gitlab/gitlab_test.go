// Package gitlab provides an adapter for interacting with GitLab.
// It facilitates operations such as creating, fetching, and deleting projects,
// as well as managing project variables and runners.
package gitlab

import (
	"reflect"
	"testing"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/vcp"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

func TestNewAdapter(t *testing.T) {
	type args struct {
		token string
	}
	tests := []struct {
		name    string
		args    args
		want    *Adapter
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewAdapter(tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewAdapter() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAdapter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdapter_GetProject(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantProject *domain.GitProject
		wantErr     bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			gotProject, err := adapter.GetProject(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.GetProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProject, tt.wantProject) {
				t.Errorf("Adapter.GetProject() = %v, want %v", gotProject, tt.wantProject)
			}
		})
	}
}

func TestAdapter_CreateProject(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		name        string
		projectType domain.ProjectType
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantProject *domain.GitProject
		wantErr     bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			gotProject, err := adapter.CreateProject(tt.args.name, tt.args.projectType)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.CreateProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProject, tt.wantProject) {
				t.Errorf("Adapter.CreateProject() = %v, want %v", gotProject, tt.wantProject)
			}
		})
	}
}

func TestAdapter_DeleteProject(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		project *domain.GitProject
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			if err := adapter.DeleteProject(tt.args.project); (err != nil) != tt.wantErr {
				t.Errorf("Adapter.DeleteProject() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAdapter_CreateRunner(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		prj *domain.GitProject
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantRunner *domain.Runner
		wantErr    bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			gotRunner, err := adapter.CreateRunner(tt.args.prj)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.CreateRunner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotRunner, tt.wantRunner) {
				t.Errorf("Adapter.CreateRunner() = %v, want %v", gotRunner, tt.wantRunner)
			}
		})
	}
}

func TestAdapter_SetProjectVariables(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		serverResource *domain.ProjectServerResource
		project        *domain.GitProject
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			if err := adapter.SetProjectVariables(tt.args.serverResource, tt.args.project); (err != nil) != tt.wantErr {
				t.Errorf("Adapter.SetProjectVariables() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_parseError(t *testing.T) {
	type args struct {
		err *error
	}
	tests := []struct {
		name string
		args args
		want *vcp.VCPError
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseError(tt.args.err); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseError() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdapter_createEmptyProject(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantProject *domain.GitProject
		wantErr     bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			gotProject, err := adapter.createEmptyProject(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.createEmptyProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProject, tt.wantProject) {
				t.Errorf("Adapter.createEmptyProject() = %v, want %v", gotProject, tt.wantProject)
			}
		})
	}
}

func TestAdapter_createEmbeddedOSLinuxProject(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantProject *domain.GitProject
		wantErr     bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			gotProject, err := adapter.createEmbeddedOSLinuxProject(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.createEmbeddedOSLinuxProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProject, tt.wantProject) {
				t.Errorf("Adapter.createEmbeddedOSLinuxProject() = %v, want %v", gotProject, tt.wantProject)
			}
		})
	}
}

func TestAdapter_convertNameToPath(t *testing.T) {
	type fields struct {
		client            *gitlab.Client
		defaultRunnerType string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantPath string
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:            tt.fields.client,
				defaultRunnerType: tt.fields.defaultRunnerType,
			}
			if gotPath := adapter.convertNameToPath(tt.args.name); gotPath != tt.wantPath {
				t.Errorf("Adapter.convertNameToPath() = %v, want %v", gotPath, tt.wantPath)
			}
		})
	}
}
