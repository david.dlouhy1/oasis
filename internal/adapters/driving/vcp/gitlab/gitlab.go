// Package gitlab provides an adapter for interacting with GitLab.
// It facilitates operations such as creating, fetching, and deleting projects,
// as well as managing project variables and runners.
package gitlab

import (
	"strconv"
	"strings"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/vcp"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

// Adapter represents the GitLab adapter.
type Adapter struct {
	client            *gitlab.Client
	defaultRunnerType string
}

// NewAdapter creates a new GitLab adapter with the provided access token.
func NewAdapter(token string) (*Adapter, error) {
	client, err := gitlab.NewClient(token)
	if err != nil {
		return nil, err
	}
	return &Adapter{
		client:            client,
		defaultRunnerType: "project_type",
	}, nil
}

// GetProject retrieves information about a GitLab project by its name.
func (adapter *Adapter) GetProject(name string) (project *domain.GitProject, err error) {
	user, _, err := adapter.client.Users.CurrentUser()
	if err != nil {
		return nil, err
	}
	pathName := user.Username + "/" + adapter.convertNameToPath(name)
	prj, _, err := adapter.client.Projects.GetProject(pathName, nil)
	if err != nil {
		return nil, err
	}
	uuid := strconv.Itoa(prj.ID)
	project = &domain.GitProject{
		UUID:  uuid,
		Owner: prj.Owner.Username,
		Name:  prj.Name,
	}
	return
}

// CreateProject creates a new GitLab project with the specified name and type.
func (adapter *Adapter) CreateProject(name string, projectType domain.ProjectType) (project *domain.GitProject, err error) {
	switch projectType {
	case "":
		project, err = adapter.createEmptyProject(name)
	case domain.PROJECT_TYPE_EMBEDDED_OS_LINUX:
		project, err = adapter.createEmbeddedOSLinuxProject(name)
	default:
		panic("not implemented")
	}
	return
}

// DeleteProject deletes the specified GitLab project.
func (adapter *Adapter) DeleteProject(project *domain.GitProject) error {
	_, err := adapter.client.Projects.DeleteProject(project.UUID)
	return err
}

// CreateRunner creates a GitLab runner for the specified project.
func (adapter *Adapter) CreateRunner(prj *domain.GitProject) (runner *domain.Runner, err error) {
	runnerDescription := "Runner created by Oasis"
	sharedRunnersEnabled := false
	prjID, err := strconv.Atoi(prj.UUID)
	if err != nil {
		return nil, err
	}
	r, _, err := adapter.client.Users.CreateUserRunner(&gitlab.CreateUserRunnerOptions{
		RunnerType:  &adapter.defaultRunnerType,
		ProjectID:   &prjID,
		Description: &runnerDescription,
	})
	if err != nil {
		return nil, err
	}

	runner = &domain.Runner{
		Project:     *prj,
		Name:        "gitlab-runner-oasis-" + prj.UUID,
		Host:        "https://gitlab.com/",
		Token:       r.Token,
		Description: runnerDescription,
	}

	_, _, err = adapter.client.Projects.EditProject(runner.Project.UUID, &gitlab.EditProjectOptions{
		SharedRunnersEnabled: &sharedRunnersEnabled,
	})
	if err != nil {
		return nil, err
	}

	return
}

// SetProjectVariables sets variables for the specified GitLab project.
func (adapter *Adapter) SetProjectVariables(serverResource *domain.ProjectServerResource, project *domain.GitProject) error {
	uuid, err := strconv.Atoi(project.UUID)
	if err != nil {
		return err
	}
	variables := map[string]string{
		"OASIS_AUTHORIZED_KEYS":         serverResource.AuthorizedKeys,
		"OASIS_DEPLOY_FOLDER":           serverResource.DeployFolder,
		"OASIS_DEPLOY_HOST":             serverResource.DeployHost,
		"OASIS_DEPLOY_USER":             serverResource.DeployUser,
		"OASIS_SSH_KEY":                 serverResource.SSHKey,
		"TESTING_OASIS_DEPLOY_PORT":     serverResource.DeployPortTest,
		"STAGING_OASIS_DEPLOY_PORT":     serverResource.DeployPortStaging,
		"TESTING_OASIS_MONITORING_PORT": serverResource.MonitoringPortTest,
		"STAGING_OASIS_MONITORING_PORT": serverResource.MonitoringPortStaging,
	}

	for key, value := range variables {
		var keyType string
		if strings.HasPrefix(key, "TESTING_") {
			keyType = "testing"
			key = strings.TrimPrefix(key, "TESTING_")
			_, _, err = adapter.client.ProjectVariables.CreateVariable(uuid, &gitlab.CreateProjectVariableOptions{Key: &key, Value: &value, EnvironmentScope: &keyType})
		} else if strings.HasPrefix(key, "STAGING_") {
			keyType = "staging"
			key = strings.TrimPrefix(key, "STAGING_")
			_, _, err = adapter.client.ProjectVariables.CreateVariable(uuid, &gitlab.CreateProjectVariableOptions{Key: &key, Value: &value, EnvironmentScope: &keyType})
		} else {
			_, _, err = adapter.client.ProjectVariables.CreateVariable(uuid, &gitlab.CreateProjectVariableOptions{Key: &key, Value: &value})
		}
		if err != nil && !strings.Contains(err.Error(), "has already been taken") {
			return err
		}
	}
	return nil
}

// parseError parses GitLab API errors and converts them into VCP errors.
func parseError(err *error) *vcp.VCPError {
	keywords := []string{"name", "path", "project_namespace.name", "message"}
	for _, keyword := range keywords {
		if strings.Contains((*err).Error(), keyword+": [has already been taken]") {
			return vcp.NewVCPError(vcp.ERROR_PROJECT_EXISTS)
		}
		if strings.Contains((*err).Error(), keyword+": [Project namespace name has already been taken, Name has already been taken, Path has already been taken]") {
			return vcp.NewVCPError(vcp.ERROR_PROJECT_EXISTS)
		}
	}
	return vcp.NewVCPError(vcp.ERROR_PROJECT_UNDEFINED)
}

// createEmptyProject creates an empty GitLab project.
func (adapter *Adapter) createEmptyProject(name string) (project *domain.GitProject, err error) {
	createdProject, _, err := adapter.client.Projects.CreateProject(&gitlab.CreateProjectOptions{
		Name: &name,
	})
	if err != nil {
		switch parseError(&err).ErrorType {
		case vcp.ERROR_PROJECT_EXISTS:
			return nil, nil
		default:
			return nil, err
		}

	}
	project = &domain.GitProject{
		UUID:  strconv.FormatInt(int64(createdProject.ID), 10),
		Owner: createdProject.Owner.Name,
		Name:  createdProject.Name,
	}
	return
}

// createEmbeddedOSLinuxProject creates a GitLab project based on an embedded OS Linux project.
func (adapter *Adapter) createEmbeddedOSLinuxProject(name string) (project *domain.GitProject, err error) {
	path := adapter.convertNameToPath(name)

	createdProject, _, err := adapter.client.Projects.ForkProject(domain.PROJECT_FORK_ID_EMBEDDED_OS_LINUX, &gitlab.ForkProjectOptions{
		Name: &name,
		Path: &path,
	})
	if err != nil {
		switch parseError(&err).ErrorType {
		case vcp.ERROR_PROJECT_EXISTS:
			return nil, nil
		default:
			return nil, err
		}

	}
	project = &domain.GitProject{
		UUID:  strconv.FormatInt(int64(createdProject.ID), 10),
		Owner: createdProject.Owner.Name,
		Name:  createdProject.Name,
	}
	return
}

// convertNameToPath converts a project name to a GitLab-compatible path.
func (adapter *Adapter) convertNameToPath(name string) (path string) {
	path = strings.ToLower(name)
	path = strings.ReplaceAll(path, " ", "-")
	return
}
