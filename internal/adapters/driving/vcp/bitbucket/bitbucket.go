// Package bitbucket provides an adapter for interacting with Bitbucket.
// It facilitates operations such as creating, fetching, and deleting projects.
package bitbucket

import (
	"fmt"
	"strings"

	"github.com/ktrysmt/go-bitbucket"
	"gitlab.com/david.dlouhy1/oasis/internal/adapters/driving/vcp"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

// Adapter represents the Bitbucket adapter.
type Adapter struct {
	client    *bitbucket.Client    // Bitbucket client instance.
	user      *bitbucket.User      // User associated with the Bitbucket client.
	workspace *bitbucket.Workspace // Workspace associated with the Bitbucket client.
}

// NewAdapter creates a new Bitbucket adapter with the provided credentials.
func NewAdapter(username string, password string, workspaceName string) (*Adapter, error) {
	client := bitbucket.NewBasicAuth(username, password)
	user, err := client.User.Profile()
	if err != nil {
		return nil, err
	}
	userWorkspace, err := client.Workspaces.Get(workspaceName)
	return &Adapter{
		client:    client,
		user:      user,
		workspace: userWorkspace,
	}, err
}

// GetProject retrieves information about a Bitbucket project by its name.
func (adapter *Adapter) GetProject(name string) (project *domain.GitProject, err error) {
	panic("not implemented")
}

// CreateProject creates a new Bitbucket project with the specified name and type.
func (adapter *Adapter) CreateProject(name string, projectType domain.ProjectType) (project *domain.GitProject, err error) {
	createdProject, err := adapter.client.Repositories.Repository.Create(&bitbucket.RepositoryOptions{Owner: adapter.workspace.Slug, RepoSlug: convertNameToRepoName(name), Scm: "git"})
	if err != nil {
		switch parseError(&err).ErrorType {
		case vcp.ERROR_PROJECT_EXISTS:
			return nil, nil
		default:
			return nil, err
		}

	}
	project = &domain.GitProject{
		UUID:  createdProject.Uuid,
		Owner: fmt.Sprint(createdProject.Owner["username"]),
		Name:  createdProject.Name,
	}
	return
}

// DeleteProject deletes the specified Bitbucket project.
func (adapter *Adapter) DeleteProject(project *domain.GitProject) error {
	_, err := adapter.client.Repositories.Repository.Delete(&bitbucket.RepositoryOptions{Owner: project.Owner, RepoSlug: project.Name})
	return err
}

// convertNameToRepoName converts a project name to a Bitbucket-compatible repository name.
func convertNameToRepoName(name string) (repoName string) {
	repoName = strings.ToLower(name)
	repoName = strings.ReplaceAll(repoName, " ", "")
	return
}

// parseError parses Bitbucket API errors and converts them into VCP errors.
func parseError(err *error) *vcp.VCPError {
	if strings.Contains((*err).(*bitbucket.UnexpectedResponseStatusError).ErrorWithBody().Error(), "Repository with this Slug and Owner already exists") {
		return vcp.NewVCPError(vcp.ERROR_PROJECT_EXISTS)
	}
	return nil
}
