// Package vcp provides functionality for managing Version Control Projects (VCPs).
package vcp

// Constants representing error types within the VCP package.
const (
	ERROR_PROJECT_UNDEFINED = 0        // ERROR_PROJECT_UNDEFINED indicates that the project is undefined.
	ERROR_PROJECT_EXISTS    = iota + 1 // ERROR_PROJECT_EXISTS indicates that the project already exists.
)

// VCPError represents errors that can occur within the VCP package.
type VCPError struct {
	ErrorType int // ErrorType represents the type of error that occurred.
}

// NewVCPError creates a new VCPError instance with the specified error type.
func NewVCPError(errorType int) *VCPError {
	return &VCPError{
		ErrorType: errorType,
	}
}
