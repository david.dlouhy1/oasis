// Package vcp provides functionality for managing Version Control Projects (VCPs).
package vcp

import (
	"reflect"
	"testing"
)

func TestNewVCPError(t *testing.T) {
	type args struct {
		errorType int
	}
	tests := []struct {
		name string
		args args
		want *VCPError
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewVCPError(tt.args.errorType); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewVCPError() = %v, want %v", got, tt.want)
			}
		})
	}
}
