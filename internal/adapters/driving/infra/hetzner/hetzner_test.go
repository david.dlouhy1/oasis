// Package hetzner provides an adapter for interacting with Hetzner Cloud.
// It facilitates operations such as creating machines and setting up SSH keys.
package hetzner

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/hetznercloud/hcloud-go/v2/hcloud"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"golang.org/x/crypto/ssh"
)

func TestNewAdapter(t *testing.T) {
	type args struct {
		token string
	}
	tests := []struct {
		name        string
		args        args
		wantAdapter *Adapter
		wantErr     bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotAdapter, err := NewAdapter(tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewAdapter() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotAdapter, tt.wantAdapter) {
				t.Errorf("NewAdapter() = %v, want %v", gotAdapter, tt.wantAdapter)
			}
		})
	}
}

func TestNewSSHTask(t *testing.T) {
	type args struct {
		name    string
		command string
	}
	tests := []struct {
		name string
		args args
		want *SSHTask
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewSSHTask(tt.args.name, tt.args.command); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewSSHTask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdapter_SetPublicSSHKey(t *testing.T) {
	type fields struct {
		Client         *hcloud.Client
		Context        *context.Context
		ServiceKeyName string
	}
	type args struct {
		publicKey string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				Client:         tt.fields.Client,
				Context:        tt.fields.Context,
				ServiceKeyName: tt.fields.ServiceKeyName,
			}
			if err := adapter.SetPublicSSHKey(tt.args.publicKey); (err != nil) != tt.wantErr {
				t.Errorf("Adapter.SetPublicSSHKey() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAdapter_CreateMachine(t *testing.T) {
	type fields struct {
		Client         *hcloud.Client
		Context        *context.Context
		ServiceKeyName string
	}
	type args struct {
		name    string
		timeout time.Duration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				Client:         tt.fields.Client,
				Context:        tt.fields.Context,
				ServiceKeyName: tt.fields.ServiceKeyName,
			}
			if err := adapter.CreateMachine(tt.args.name, tt.args.timeout); (err != nil) != tt.wantErr {
				t.Errorf("Adapter.CreateMachine() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAdapter_GetMachineIP(t *testing.T) {
	type fields struct {
		Client         *hcloud.Client
		Context        *context.Context
		ServiceKeyName string
	}
	type args struct {
		serverName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantIp  string
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				Client:         tt.fields.Client,
				Context:        tt.fields.Context,
				ServiceKeyName: tt.fields.ServiceKeyName,
			}
			gotIp, err := adapter.GetMachineIP(tt.args.serverName)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.GetMachineIP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotIp != tt.wantIp {
				t.Errorf("Adapter.GetMachineIP() = %v, want %v", gotIp, tt.wantIp)
			}
		})
	}
}

func TestAdapter_CreateServer(t *testing.T) {
	type fields struct {
		Client         *hcloud.Client
		Context        *context.Context
		ServiceKeyName string
	}
	type args struct {
		privateKey string
		publicKey  string
		runner     *domain.Runner
	}
	tests := []struct {
		name                    string
		fields                  fields
		args                    args
		wantProjectSrvResources *domain.ProjectServerResource
		wantErr                 bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				Client:         tt.fields.Client,
				Context:        tt.fields.Context,
				ServiceKeyName: tt.fields.ServiceKeyName,
			}
			gotProjectSrvResources, err := adapter.CreateServer(tt.args.privateKey, tt.args.publicKey, tt.args.runner)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.CreateServer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProjectSrvResources, tt.wantProjectSrvResources) {
				t.Errorf("Adapter.CreateServer() = %v, want %v", gotProjectSrvResources, tt.wantProjectSrvResources)
			}
		})
	}
}

func TestAdapter_combinedOutput(t *testing.T) {
	type fields struct {
		Client         *hcloud.Client
		Context        *context.Context
		ServiceKeyName string
	}
	type args struct {
		conn *ssh.Client
		cmd  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				Client:         tt.fields.Client,
				Context:        tt.fields.Context,
				ServiceKeyName: tt.fields.ServiceKeyName,
			}
			got, err := adapter.combinedOutput(tt.args.conn, tt.args.cmd)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.combinedOutput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Adapter.combinedOutput() = %v, want %v", got, tt.want)
			}
		})
	}
}
