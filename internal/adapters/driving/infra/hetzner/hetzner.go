// Package hetzner provides an adapter for interacting with Hetzner Cloud.
// It facilitates operations such as creating machines and setting up SSH keys.
package hetzner

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"github.com/hetznercloud/hcloud-go/v2/hcloud"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
	"golang.org/x/crypto/ssh"
)

// Adapter represents the Hetzner Cloud adapter.
type Adapter struct {
	Client         *hcloud.Client   // Hetzner Cloud client instance.
	Context        *context.Context // Context for adapter operations.
	ServiceKeyName string           // Name of the SSH key service.
}

// NewAdapter creates a new Hetzner Cloud adapter with the provided API token.
func NewAdapter(token string) (adapter *Adapter, err error) {

	client := hcloud.NewClient(hcloud.WithToken(token))
	ctx := context.Background()
	adapter = &Adapter{
		Client:         client,
		Context:        &ctx,
		ServiceKeyName: "OasisKey",
	}
	return
}

// SSHTask represents an SSH task to be executed on a server.
type SSHTask struct {
	Name    string // Name of the task.
	Command string // SSH command to execute.
}

// NewSSHTask creates a new SSH task with the provided name and command.
func NewSSHTask(name string, command string) *SSHTask {
	return &SSHTask{
		Name:    name,
		Command: command,
	}
}

// SetPublicSSHKey sets the public SSH key for the Hetzner Cloud adapter.
func (adapter *Adapter) SetPublicSSHKey(publicKey string) (err error) {
	opts := hcloud.SSHKeyCreateOpts{
		Name:      adapter.ServiceKeyName,
		PublicKey: publicKey,
	}
	_, _, err = adapter.Client.SSHKey.Create(*adapter.Context, opts)
	return
}

// CreateMachine creates a new machine with the specified name and timeout.
func (adapter *Adapter) CreateMachine(name string, timeout time.Duration) (err error) {

	sshKey, _, err := adapter.Client.SSHKey.GetByName(*adapter.Context, adapter.ServiceKeyName)
	if err != nil {
		return
	}

	createOpts := hcloud.ServerCreateOpts{
		Name:       name,
		SSHKeys:    []*hcloud.SSHKey{sshKey},
		ServerType: &hcloud.ServerType{Name: "cpx41"},
		Image:      &hcloud.Image{Name: "rocky-9"},
	}
	server, _, err := adapter.Client.Server.Create(*adapter.Context, createOpts)
	if err != nil && !strings.Contains(err.Error(), "server name is already used") {
		return
	}
	if server.Server == nil {
		getServerResult, _, err := adapter.Client.Server.GetByName(*adapter.Context, name)
		if err != nil || getServerResult == nil {
			return err
		}
		server = hcloud.ServerCreateResult{Server: getServerResult}
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	processDone := make(chan bool)
	go func() {
		for server.Server.Status != hcloud.ServerStatusRunning {
			time.Sleep(time.Second * 3)
			getServerResult, _, err := adapter.Client.Server.GetByID(*adapter.Context, server.Server.ID)
			if err != nil || getServerResult == nil {
				return
			}
			server = hcloud.ServerCreateResult{Server: getServerResult}
		}
		processDone <- true
	}()

	select {
	case <-ctx.Done():
		return errors.New("timeout")
	case <-processDone:
	}
	return err
}

// GetMachineIP retrieves the IP address of the machine with the specified name.
func (adapter *Adapter) GetMachineIP(serverName string) (ip string, err error) {
	server, _, err := adapter.Client.Server.GetByName(*adapter.Context, serverName)
	if err != nil {
		return
	}
	ip = server.PublicNet.IPv4.IP.String()
	return
}

// CreateServer creates a new server with the provided private and public keys, and runner configuration.
func (adapter *Adapter) CreateServer(privateKey string, publicKey string, runner *domain.Runner) (projectSrvResources *domain.ProjectServerResource, err error) {
	serverName := "buildServer"
	projectSrvResources = &domain.ProjectServerResource{}
	err = adapter.CreateMachine(serverName, time.Minute*5)

	//TOOTO BY ASI M12LO BˇYT V SSH ADAPTERU
	if err != nil && !strings.Contains(err.Error(), "server name is already used") {
		return
	}
	signer, err := ssh.ParsePrivateKey([]byte(privateKey))
	if err != nil {
		return
	}

	sshConf := &ssh.ClientConfig{
		User:            "root",
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
	}

	serverIp, err := adapter.GetMachineIP(serverName)
	if err != nil {
		return
	}

	conn, err := ssh.Dial("tcp", serverIp+":"+"22", sshConf)
	if err != nil {
		return
	}
	defer conn.Close()

	commands := []SSHTask{}
	commands = append(commands, *NewSSHTask("Update packages", "dnf update -y"))
	commands = append(commands, *NewSSHTask("Upgrade system", "dnf upgrade -y"))
	commands = append(commands, *NewSSHTask("Install DNF Plugins Core", "dnf install -y dnf-plugins-core"))
	commands = append(commands, *NewSSHTask("Install EPEL", "dnf install -y epel-release"))
	commands = append(commands, *NewSSHTask("Install base tools", "dnf install -y htop vim curl wget git rsync mkpasswd"))
	commands = append(commands, *NewSSHTask("Add Docker repo", "dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo"))
	commands = append(commands, *NewSSHTask("Install Docker", "dnf -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin"))
	commands = append(commands, *NewSSHTask("Enable and start Docker", "systemctl --now enable docker"))
	commands = append(commands, *NewSSHTask("Create deployment folder", "mkdir -p /srv/deployment"))
	commands = append(commands, *NewSSHTask("Create staging folder", "mkdir -p /srv/deployment/staging"))
	commands = append(commands, *NewSSHTask("Create test folder", "mkdir -p /srv/deployment/testing"))

	projectSrvResources.DeployFolder = "/srv/deployment"
	projectSrvResources.SSHKey = privateKey
	projectSrvResources.AuthorizedKeys = publicKey
	projectSrvResources.DeployHost = serverIp
	projectSrvResources.DeployUser = "root"

	freePortCmd := (*NewSSHTask("Detect free ports ", "comm -23 <(seq 49152 65535 | sort) <(ss -Htan | awk '{print $4}' | cut -d':' -f2 | sort -u) | shuf | head -n 4"))
	mess, err := adapter.combinedOutput(conn, freePortCmd.Command)
	if err != nil {
		log.Printf("Task %s failed: %s\n", freePortCmd.Name, mess)
		log.Println(err)
		return nil, err
	}
	freePorts := strings.Split(mess, "\n")
	projectSrvResources.DeployPortStaging = freePorts[0]
	projectSrvResources.DeployPortTest = freePorts[1]
	projectSrvResources.MonitoringPortStaging = freePorts[2]
	projectSrvResources.MonitoringPortTest = freePorts[3]

	commands = append(commands, *NewSSHTask("Prepare runner folder", "mkdir -p /srv/"+runner.Name))
	commands = append(commands, *NewSSHTask("Prepare DL_DIR", "mkdir /srv/storage/yocto/dl_dir -p"))
	commands = append(commands, *NewSSHTask("Prepare SS_DIR", "mkdir /srv/storage/yocto/sstate-cache -p"))
	commands = append(commands, *NewSSHTask("Change mod DL_DIR", "chmod 777 /srv/storage/yocto/dl_dir -R"))
	commands = append(commands, *NewSSHTask("Change SS_DIR", "chmod 777 /srv/storage/yocto/sstate-cache -R"))

	commands = append(commands, *NewSSHTask("Register GitLab runner", "docker start "+runner.Name+" || docker run --rm --name "+runner.Name+" -v /srv/"+runner.Name+"/config:/etc/gitlab-runner gitlab/gitlab-runner register --non-interactive --url \""+runner.Host+"\" --token \""+runner.Token+"\" --executor \"docker\" --docker-image alpine:latest --description \""+runner.Description+"\" --docker-privileged --docker-volumes /srv/storage:/srv/storage "))

	commands = append(commands, *NewSSHTask("Deploy GitLab runner", "docker start "+runner.Name+" || docker run -d --name "+runner.Name+" --restart always -v /srv/"+runner.Name+"/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest"))

	containerPrometheusName := "prometheus"
	containerNodeExporterName := "node-exporter"
	containerGrafanaName := "grafana"
	commands = append(commands, *NewSSHTask("Create monitor folder", "mkdir -p /srv/prometheus"))
	commands = append(commands, *NewSSHTask("Create monitor folder", "mkdir -p /srv/grafana"))
	commands = append(commands, *NewSSHTask("Configure Prometheus (global)", "echo \"global:\n  scrape_interval: 15s\n  external_labels:\n    monitor: 'codelab-monitor'\" > /srv/prometheus/prometheus.yml"))
	commands = append(commands, *NewSSHTask("Configure Prometheus (basic scrape)", "echo \"scrape_configs:\n  - job_name: 'prometheus'\n    scrape_interval: 5s\n    static_configs:\n      - targets: ['localhost:9090']\n  - job_name: '"+runner.Project.Name+"'\n    scrape_interval: 5s\n    static_configs:\n      - targets: ['"+serverIp+":9100']\" >> /srv/prometheus/prometheus.yml"))
	commands = append(commands, *NewSSHTask("Configure Prometheus (project)", "echo \"  - job_name: 'staging'\n    scrape_interval: 5s\n    static_configs:\n      - targets: ['"+serverIp+":"+projectSrvResources.MonitoringPortStaging+"']\" >> /srv/prometheus/prometheus.yml"))
	commands = append(commands, *NewSSHTask("Deploy Prometheus", "docker restart "+containerPrometheusName+" || docker run -d --name "+containerPrometheusName+" --restart always -p 9090:9090 -v /srv/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus"))
	commands = append(commands, *NewSSHTask("Deploy Node Exporter", "docker start "+containerNodeExporterName+" || docker run -d --name "+containerNodeExporterName+" --restart always -p 9100:9100  -v '/:/host:ro,rslave' --net='host' --pid='host' prom/node-exporter --path.rootfs=/host"))
	commands = append(commands, *NewSSHTask("Deploy Grafana", "docker start "+containerGrafanaName+" || docker run -d --name "+containerGrafanaName+" --restart always -p 3000:3000 -v /srv/grafana/provisioning:/etc/grafana/provisioning -v /srv/grafana/grafana/dashboards:/var/lib/grafana/dashboards -v grafana_data:/var/lib/grafana grafana/grafana:10.1.7"))

	for i, command := range commands {
		taskNumber := i + 1
		log.Printf("Starting SSH task [%d/%d]: %s\n", taskNumber, len(commands), command.Name)
		mess, err := adapter.combinedOutput(conn, command.Command)
		if err != nil {
			log.Printf("Task %s failed: %s\n", command.Name, mess)
			log.Println(err)
			return nil, err
		}
		log.Printf("OK SSH task [%d/%d]: %s\n", taskNumber, len(commands), command.Name)
		log.Println(mess)
	}

	return
}

// combinedOutput executes the provided SSH command and returns combined stdout and stderr output.
func (adapter *Adapter) combinedOutput(conn *ssh.Client, cmd string) (string, error) {
	sess, err := conn.NewSession()
	if err != nil {
		return "", err
	}
	defer sess.Close()
	b, err := sess.Output(cmd)
	if err != nil {
		return string(b), err
	}
	return string(b), nil
}
