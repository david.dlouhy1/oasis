// Package vmware provides an adapter for interacting with VMware vSphere.
// It facilitates operations such as creating locations and virtual machines.
package vmware

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"strings"

	"github.com/vmware/govmomi"
	"github.com/vmware/govmomi/find"
	"github.com/vmware/govmomi/object"
	"github.com/vmware/govmomi/vim25/methods"
	"github.com/vmware/govmomi/vim25/mo"
	"github.com/vmware/govmomi/vim25/types"
)

// Adapter represents the VMware vSphere adapter.
type Adapter struct {
	Username   string              // Username for connecting to VMware vSphere.
	Password   string              // Password for connecting to VMware vSphere.
	Hostname   string              // Hostname for connecting to VMware vSphere.
	Path       string              // Path to the target location.
	Datacenter string              // Datacenter where the operations will be performed.
	Client     *govmomi.Client     // VMware vSphere client instance.
	Context    *context.Context    // Context for adapter operations.
	cancel     *context.CancelFunc // Cancel function for adapter operations.
}

// NewAdapter creates a new VMware vSphere adapter with the provided credentials.
func NewAdapter(username string, password string, hostname string, path string) (adapter *Adapter, err error) {

	u, err := url.Parse("https://" + username + ":" + password + "@" + hostname + "/sdk")
	if err != nil {
		return
	}
	pathSplit := strings.Split(path, "/")
	ctx, cancel := context.WithCancel(context.Background())
	client, err := govmomi.NewClient(ctx, u, true)
	if err != nil {
		defer cancel()
		return
	}
	adapter = &Adapter{
		Username:   username,
		Password:   password,
		Hostname:   hostname,
		Client:     client,
		Datacenter: pathSplit[1],
		Path:       path,
		Context:    &ctx,
		cancel:     &cancel,
	}
	return
}

// CreateLocation creates a new folder in the specified location.
func (adapter *Adapter) CreateLocation() (err error) {
	f := find.NewFinder(adapter.Client.Client, true)

	dcObj, err := f.DatacenterOrDefault(*adapter.Context, adapter.Datacenter)
	if err != nil {
		return
	}
	f.SetDatacenter(dcObj)

	x, err := f.Folder(*adapter.Context, adapter.Path)
	if err != nil {
		return
	}

	req := types.CreateFolder{
		This: x.Reference(),
		Name: "oasis",
	}
	res, err := methods.CreateFolder(*adapter.Context, adapter.Client, &req)
	if err != nil {
		return
	}
	helloFolder := res.Returnval
	fmt.Println(helloFolder)
	return
}

// getLocation retrieves the folder object for the specified location.
func (adapter *Adapter) getLocation() (vmFolder *object.Folder, err error) {
	// search target host
	f := find.NewFinder(adapter.Client.Client, true)

	dcObj, err := f.DatacenterOrDefault(*adapter.Context, adapter.Datacenter)
	if err != nil {
		return
	}
	f.SetDatacenter(dcObj)

	return f.Folder(*adapter.Context, adapter.Path+"/oasis")
}

// CreateMachine creates a new virtual machine.
func (adapter *Adapter) CreateMachine() {

	// search target host
	finder := find.NewFinder(adapter.Client.Client, true)
	host, err := finder.HostSystem(*adapter.Context, "/GTS-Datacenter/host/GTS-Cluster/hp5.cis.cz")
	if err != nil {
		log.Fatal(err)
	}

	// get host information
	var hostProperties mo.HostSystem
	err = host.Properties(context.Background(), host.Reference(), []string{"config.network"}, &hostProperties)
	if err != nil {
		log.Fatal(err)
	}

	// get folder store for VM
	vmFolder, err := adapter.getLocation()
	if err != nil {
		log.Fatalln(err)
	}
	// create new vm config
	var config types.VirtualMachineConfigSpec
	config.Name = "test-vm"
	config.GuestId = "other3xLinux64Guest"
	//config.Files = &types.VirtualMachineFileInfo{VmPathName: fmt.Sprintf("[%s]", hostProperties.Config.Network.Vswitch[0].Name)}
	config.NumCPUs = 2
	config.MemoryMB = 4096

	pool, err := host.ResourcePool(*adapter.Context)

	// create ew vm with previous parameters
	vm, err := vmFolder.CreateVM(*adapter.Context, config, pool, nil)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(vm)

}
