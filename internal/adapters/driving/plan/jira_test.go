// Package jira provides an adapter for interacting with Jira.
// It facilitates operations such as creating plans.
package jira

import (
	"reflect"
	"testing"

	v3 "github.com/ctreminiom/go-atlassian/jira/v3"
	"gitlab.com/david.dlouhy1/oasis/internal/domain"
)

func TestNewAdapter(t *testing.T) {
	type args struct {
		token string
		url   string
		mail  string
	}
	tests := []struct {
		name    string
		args    args
		want    *Adapter
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewAdapter(tt.args.token, tt.args.url, tt.args.mail)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewAdapter() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAdapter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdapter_CreatePlan(t *testing.T) {
	type fields struct {
		client             *v3.Client
		defaultTemplate    string
		defaultDescription string
	}
	type args struct {
		name string
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantProject *domain.Plan
		wantErr     bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			adapter := &Adapter{
				client:             tt.fields.client,
				defaultTemplate:    tt.fields.defaultTemplate,
				defaultDescription: tt.fields.defaultDescription,
			}
			gotProject, err := adapter.CreatePlan(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Adapter.CreatePlan() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProject, tt.wantProject) {
				t.Errorf("Adapter.CreatePlan() = %v, want %v", gotProject, tt.wantProject)
			}
		})
	}
}
