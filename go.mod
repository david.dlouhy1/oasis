module gitlab.com/david.dlouhy1/oasis

go 1.22.0

require (
	github.com/goccy/go-graphviz v0.1.2
	github.com/hetznercloud/hcloud-go/v2 v2.7.2
	github.com/ktrysmt/go-bitbucket v0.9.73
	github.com/spf13/cobra v1.8.0
	github.com/vmware/govmomi v0.37.1
)

require (
	github.com/andygrunwald/go-jira v1.16.0 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/ctreminiom/go-atlassian v1.6.0 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.4.2 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/imdario/mergo v0.3.16 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_golang v1.19.0 // indirect
	github.com/prometheus/client_model v0.5.0 // indirect
	github.com/prometheus/common v0.48.0 // indirect
	github.com/prometheus/procfs v0.12.0 // indirect
	github.com/tidwall/gjson v1.17.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/trivago/tgo v1.0.7 // indirect
	golang.org/x/image v0.14.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-github/v57 v57.0.0
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.2 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/xanzy/go-gitlab v0.95.2
	golang.org/x/crypto v0.22.0
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/oauth2 v0.16.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
)
