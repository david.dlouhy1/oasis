// Package main is the entry point for the Oasis application.
package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/david.dlouhy1/oasis/internal/app"
)

var (
	application *app.App

	rootCmd = &cobra.Command{
		Use:   "oasis",
		Short: "Oasis is a tool for managing infrastructure.",
		Long:  `Oasis is a command-line tool for managing infrastructure using a declarative configuration file.`}

	applyCmd = &cobra.Command{
		Use:   "apply",
		Short: "Prepare and execute a sequence of steps to build the infrastructure.",
		Long:  `The apply command prepares and executes a sequence of steps defined in the configuration file to build the infrastructure.`,
	}
)

// init initializes the application.
func init() {
	application = app.NewApp()

	// Set the apply command's run function to handle apply command.
	applyCmd.Run = application.ConsolePort.HandleApply

	// Add flags to the apply command.
	applyCmd.Flags().StringP("file", "f", "oasis.yml", "Oasis definition file")

	// Add the apply command to the root command.
	rootCmd.AddCommand(applyCmd)
}

// Execute runs the root command.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// main is the entry point of the application.
func main() {
	Execute()
}
